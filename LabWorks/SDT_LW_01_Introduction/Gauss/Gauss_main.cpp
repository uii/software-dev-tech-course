#include <cstdio>
#include <cfloat>
#include <math.h>

int main()
{
	const size_t MAX_ORDER = 10; // Наибольший поддерживаемый порядок системы.

	double matrix[MAX_ORDER][MAX_ORDER]; // Матрица системы А.
	double rightSide[MAX_ORDER];		 // Вектор правых частей b.
	size_t order;						 // Порядок системы N.


	do
	{
		printf("Enter the system order (1 to %u): ", MAX_ORDER);
		scanf("%u", &order);
	}
	while (order <= 0 || MAX_ORDER < order);

	puts("Enter system matrix (A), line by line, separating elements with spaces:");
	for (size_t row = 0; row < order; ++row)
		for (size_t column = 0; column < order; ++column)
			scanf("%lf", &matrix[row][column]);

	puts("Enter the right side vector (either as a row or a column):");
	for (size_t index = 0; index < order; ++index)
		scanf("%lf", &rightSide[index]);


    for (size_t row = 0; row < order - 1; row++)
	{
		// Признак, что можно занулить данный столбец
		// путем вычитания текущей строки.
		bool canEliminateColumn = true;

		// Константа DBL_EPSILON из <cfloat> означает "машинное эпсилон"
		// для типа double: 	min eps : (1.0 + eps) > 1.0
		if (fabs(matrix[row][row]) < DBL_EPSILON)
		{
			// Если диагональный элемент текущей строки очень мал (почти 0),
			// нужно поменять строку местами с другой, где он больше (по модулю).
			// Подходящий ряд для замены нужно отыскать.
			canEliminateColumn = false;
			for (size_t replacement = row + 1; replacement < order; replacement++)
			{
				if (fabs(matrix[replacement][row]) >= DBL_EPSILON)
				{
					for (size_t column = 0; column < order; column++)
					{
						// Три строки: обмен значениями переменных.
						double temporary = matrix[row][column];
						matrix[row][column] = matrix[replacement][column];
						matrix[replacement][column] = temporary;
					}
					double temporary = rightSide[row];
					rightSide[row] = rightSide[replacement];
					rightSide[replacement] = temporary;
					canEliminateColumn = true;

					// Прерывает цикл for.
					break;
				}
			}
		}

		if (canEliminateColumn)
		{
			// Зануление текущего столбца: последовательное вычитание
			// текущей строки (с коэффициентом) из последующих.
			for (size_t otherRow = row + 1; otherRow < order; otherRow++)
			{
				double factor = matrix[otherRow][row] / matrix[row][row];
				for (size_t column = 0; column < order; column++)
				{
					matrix[otherRow][column] -= matrix[row][column] * factor;
				}
				rightSide[otherRow] -= rightSide[row] * factor;
			}
		}
		else
		{
			// Зануление столбца невозможно -- выход из программы.
			puts("Failed to diagonalize matrix!");
			return 1;
		}
	}

	// ВАЖНО: Будем считать (для простоты), что ранг системы
	//		  равен ее порядку (свободных переменных нет),
	//		  и система "хорошая" (деления на нуль не случается).
	double results[MAX_ORDER];
	results[order - 1] = rightSide[order - 1] / matrix[order - 1][order - 1];
	for (int variable = order - 2; variable >= 0; variable--)
	{
		double sum = rightSide[variable];
		for (size_t other = variable + 1; other < order; other++)
		{
			sum -= matrix[variable][other] * results[other];
		}
		results[variable] = sum / matrix[variable][variable];
	}


	printf("Solution is [");
	for (size_t i = 0; i < order; i++)
		printf("%.2f%s", results[i], (i < order - 1) ? "; " : "");
	printf("].");

    return 0;
}
