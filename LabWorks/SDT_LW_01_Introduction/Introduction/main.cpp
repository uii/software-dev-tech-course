/*
	Следующие строки, начинающиеся с октоторпа (#), называются называются
	директивами препроцессора (preprocessor instructions). Они исполняются
	перед компиляцией и служат для различных преобразований текста программы.

	Директива include приводит ко включению в текст данного файла содержимого
	файла, указанного параметром директивы. Если скобки угловые (<>), поиск
	включаемого файла производится в специальных каталогах (настраиваются
	в меню Project -> Properties -> Configuration Properties, разделы
	VC++ Directories и General, пункты со словом "Include"). Если вместо скобок
	использованы двойные кавычки (""), в них указывается путь ко включаемому файлу
	относительно текущего.

	Включение файлов НЕ аналогично uses в Delphi, это именно текстовая вставка.
	Механизма модулей в С++ нет. Директива include может находиться на любой
	строке файла, просто принято перечислять зависимости в начале.
*/
#include <cstdint>	// Некоторые платформонезависимые типы данных.
#include <cstdio>	// Поддержка ввода-вывода в стиле С.
#include <math.h>	// Математические функции и константы.

#include "power.h"	// Прототипы (см. ниже) функций вычисления степени.


// Прототипы некоторых используемых функций (аналогично предварительному
// объявлению подпрограмм в Delphi с ключевым словом forward).
//
// 1)	Тип возвращаемого значения void (cм. main() ниже) указывает,
//		что функции ничего не возвращают (как процедуры в Delphi).
// 2)	Пустые круглые скобки означают отсутствие аргументов и обязательны,
//		допускается даже ставить в них void, чтобы подчеркнуть это.
void present_expressions();
void present_loops();
void present_functions();
void present_arrays();
void present_recusion_and_stack_overflow();
void break_line();
void print_as_binary(const uint8_t byte); // Реализацию см. в print_as_binary.cpp


// Определение функции main(), с которой начинается выполнение программы
// (как begin...end. в Pascal). Функция main() должна быть в программе единственной,
// то есть не повторяться в файлах *.cpp и не быть перегруженной (см. power()).
//
// Тип возвращаемого значения -- целое со знаком (int), оно используется
// в качестве кода завершения программы (exit code) и при норме должно быть 0.
int main()
{
	// Функция puts() печатает заданную последовательность символов (в двойных кавычках)
	// и переводит курсор на следующую строку.
	puts("C++ Tutorial Samples");
	puts("");

	// Объявление и инициализация переменной step_choice типа "целое со знаком".
	// В переменной будет храниться выбранный пользователем пункт демонстрации.
	int step_choice = -1;

	// Начало цикла do...while, подобного repeat...until в Pascal, с той разницей,
	// что используется условие продолжения, а не условие прекращения цикла.
	do
	{
		puts("Choose a sample:");

		// В строковых литералах (в двойных кавычках) можно использовать специальные
		// последовательности (escape sequences), начинающиеся с обратной косой черты.
		// Например, \t -- горизонтальная табуляция, \n -- перевод строки и т. п.
		puts("\t1) Expressions & bit operations");
		puts("\t2) Loops (multiplication table)");
		puts("\t3) Functions (power)");
		puts("\t4) Arrays");
		puts("\t5) Recursion & stack overflow");
		printf("Enter selected option number or anything else to quit: ");

		//	1)	Функция scanf() аналогична fscanf() в MATLAB, но результаты ввода
		//		записываются в переменные, перечисленные в списке параметров.
		//	2)	Амперсанды (&) перед именами переменных обязательны,
		//		объяснения -- на 2-й лекции (знающим: это взятие адреса).
		//	3)	Функция scanf() возвращает количество успешно считанных элементов.
		int read_count = scanf("%d", &step_choice);

		// Условный оператор:
		//	1)	Круглые скобки обязательны (в отличие от Pascal).
		//	2)	Оператор != означает "не равно".
		//	3)	Фигурные скобки подобны begin...end и необязательны, если в них один
		//		оператор (как в данном случае). Однако, рекомендуется ставить их всегда,
		//		чтобы не забыть добавить, если понадобится использовать внутри блока if
		//		больше операторов.
		if (read_count != 1)
		{
			break; // Приведет к немедленному выходу из цикла do...while.
		}

		puts("");

		// Переключатель передаст управление метке "case N", где N -- значение step_choice.
		switch (step_choice)
		{
		case 1:
			// Прототип функции уже объявлен (перед main()), а реализация находится ниже.
			present_expressions();

			// Приводит к передаче управления в конец блока операторов switch.
			// Если бы break отсутствовал, выполнение продолжилось бы отсюда вниз.
			break;

		case 2:
			present_loops();
			break;

		case 3:
			present_functions();
			break;

		case 4:
			present_arrays();
			break;

		case 5:
			present_recusion_and_stack_overflow();
			break;

		// Обработка значений step_choice, не вошедших в предыдущие случаи.
		default:
			// Один из способов покинуть цикл - воспользоваться оператором goto
			// (break применить нельзя, так как здесь он воздействует на switch).
			// Более элегантный способ -- завести переменную, от которой зависело бы
			// условие продолжения цикла, и изменить ее здесь (попробуйте!).
			goto finish;
		}
	}
	while (true); // Бесконечный цикл (команды выхода находятся в его теле).

finish: // Метка для оператора goto.

	return 0; // Оператор возврата значения из функции, см. также: power().
}


// Демонстрация использования условных операторов и выражений.
//
// Вообще говоря, эта функция вызывается из единственного места в программе
// и не зависит ни от чего, поэтому ее код мог бы располагаться просто на месте вызова.
// Однако, "хорошим тоном" считается вынесение функциональности в такие блоки
// размером примерно 1 экран (до 20 строк), чтобы упростить чтение кода.
void present_expressions()
{
	puts("Conditional operators & statements demo sample.");
	break_line();

	unsigned int a, b, c;
	printf("Enter three numbers (0 <= a, b, c < 256): ");
	scanf("%u %u %u", &a, &b, &c);

	puts("Here's some of their relations:");

	// Операнды конъюнкции (логического "и", &&) вычисляются слева направо.
	// Действиет правило сокращенного вычисления (short-circuit evaluation):
	// если по первому операнду можно узнать значение всего выражения,
	// второй не вычисляется. Поэтому, если b == 0, a % b никогда не выполнится
	// и не приведет к делению на нуль (% аналогично mod в Pascal).
	if (b != 0 && a % b == 0)
	{
		printf("\t%u is a proper divisor of %u.", b, a);
	}

	// Условное выражение (тернарный оператор) работает так:
	// если условие до знака вопроса истинно,
	//		результат оператора -- результат выражения слева от двоеточия,
	// иначе
	//		результат оператора -- результат выражения справа от двоеточия.
	// Выражение, которое не понадобилось, не вычисляется.
	printf("\tMaximum of A = %u and B = %u is %u.\n", a, b, a > b ? a : b);

	// Так как условное выражение вычисляется справа налево (сначала внутреннее,
	// затем внешнее), скобки можно опускать, но рекомендуется оставлять,
	// так как повышается читаемость кода.
	printf(
		"\tMaximum of A = %u, B = %u, and C = %u is %u.\n",
		a, b, c,
		a > b ? (a > c ? a : c) : (b > c ? b : c));

	puts("\tBit arithmetic yields (in binary form):");

	printf("\t\t    a = ");

	// Выражение статического приведения типа (static_cast) преобразует
	// переданное значение в указанный в угловых скобках (<>) тип
	// с проверкой правомерности такого преобразования на этапе компиляции.
	//
	// Тип uint8_t определен в стандартном заголовочном файле cstdint
	// и обозначает беззнаковое целое размером 8 бит (1 байт).
	// Важно: размеры встроенных типов С++ не фиксированы!
	//
	// С функцией print_as_binary() рекомендуется ознакомиться после циклов.
	print_as_binary(static_cast<uint8_t>(a));

	break_line();

	printf("\t\t   ~a = ");
	print_as_binary(static_cast<uint8_t>(~a));
	break_line();

	printf("\t\t    b = ");
	print_as_binary(static_cast<uint8_t>(b));
	break_line();

	printf("\t\ta & b = ");
	print_as_binary(static_cast<uint8_t>(a & b));
	break_line();

	printf("\t\ta | b = ");
	print_as_binary(static_cast<uint8_t>(a | b));
	break_line();

	printf("\t\ta ^ b = ");
	print_as_binary(static_cast<uint8_t>(a ^ b));
	break_line();

	break_line();
}


// Демонстрация использования циклов.
void present_loops()
{
	puts("Demo of loop use in order to print multiplication table.");
	break_line();

	// Объявления и инициализация неизменяемых (константных) переменных.
	//
	const int number_width = 4; // Ширина столбца таблицы с числом.
	const int screen_width = 80; // Ширина экрана по умолчанию (символов).
	const int max_bound =
		(screen_width - number_width) / number_width; // Наибольшая величина таблицы.

	// Объявление двумерного массива для хранения таблицы умножения
	// размером max_bound на max_bound (просто для демонстрации).
	// См. подробнее в функции present_arrays().
	int table[max_bound][max_bound];

	int bound = 0; // Величина таблицы умножения (от 1 до bound).
	do
	{
		printf("The table should be printed from 1 to: ");
		scanf("%d", &bound);
		if (bound <= 0)
		{
			puts("Positive upper bound required!");
		}
		else if (bound > max_bound)
		{
			puts("The table of the desired size cannot be fit on a standard 80x25 screen!");
		}
	} while (bound <= 0 || bound > max_bound);

	// Обратите внимание на использование астериска (*) в форматной строке
	// и выясните в документации, как он действует.
	// В данной функции это нужно, чтобы все отступы автомтически
	// соответствовали наперед заданной шинире столбцов таблицы number_width.
	printf("%*c", number_width, ' ');

	// Цикл for в С и С++ более функционален, чем в Pascal и Delphi.
	// В скобках находятся три части, разделенные точкой с запятой:
	//	1)	Оператор инициализации, выполняемый перед началом цикла.
	//		Переменные, объявленные в нем, видимы только в теле цикла.
	//	2)	Условие продолжения, может иметь любой вид, не обязательно неравенство.
	//		Если условие опущено, оно считается всегда истинным.
	//	3)	Выражение, вычисляемое после каждой итерации. Часто используется
	//		для перехода к следующему значению переменной-счетчика, как здесь
	//		(оператор инкремента (++) подобен встроенной подпрограмме Inc() в Delphi).
	// В теле цикла можно использовать break -- прерывание цикла --
	// и continue -- переход к следующей итерации с выполнением 3).
	for (int i = 1; i <= bound; i++)
	{
		printf("%*d", number_width, i);
	}

	printf("\n%*c+", number_width - 1, ' ');

	// Переменная i здесь объявляется заново и не имеет
	// ничего общего с переменной i из других циклов for.
	for (int i = 0; i < number_width * bound; i++)
	{
		// Функция putchar() печатает заданный символ.
		putchar('-');
	}
	break_line();

	for (int i = 1; i <= bound; i++)
	{
		printf("%*d |", number_width - 2, i);
		for (int j = 1; j <= bound; j++)
		{
			// Синтаксис обращения к элементам массива несколько иной, чем в Delphi.
			// Если случайно написать неправильно (table[i - 1, j - 1]), ошибки не будет
			// (только предупреждение компилятора о возможной опечатке), но программа
			// будет работать неправильно. Эта особенность разъяснится позднее.
			table[i - 1][j - 1] = i * j;
			printf("%*d", number_width, table[i - 1][j - 1]);
		}
		break_line();
	}

	break_line();
}


// Демонстрация работы функции вычисления целой степени числа.
void present_functions()
{
	puts("Integer exponent power function demo.");
	puts("Zero base returns control to the main menu.");
	puts("Note: even to quit, you have to enter BOTH base & exponent (i. e. 0 0)!");
	break_line();

	// Очередной способ записать бесконечный цикл: пустой оператор инициализации,
	// пустое выражение условия продолжения (считается всегда истинным)
	// и пустое выражение, выполняемое после каждой итерации.
	// Полную форму цикла for см. в функции power().
	for (;;)
	{
		double base; // Объявление переменной вещественного типа (double).
		int exponent;
		printf("Enter a base and an integer exponent: ");
		scanf("%lf %d", &base, &exponent);

		// Вообще говоря, значения типов с плавающей запятой не следует проверять
		// на строгое равенство из-за погрешностей при вычислениях.
		// Однако, здесь особый случай: вычислений нет, и 0 всегда вводится точно.
		if (base == 0)
		{
			break; // Приведет к немедленному выходу из цикла for.
		}

		// Функция printf() аналогична fprintf() в MATLAB, кроме возможности
		// работы с векторными и матричными величинами (она отсутствует).
		printf("%g ** %+d = %g\n", base, exponent, power(base, exponent));
	}
	break_line();
}

// Демонстрирует использование массивов.
void present_arrays()
{
	puts("Demo of the array use for statistical calculations.");

	const int ARRAY_SIZE = 20;

	// Объявление массива действительных чисел array размером ARRAY_SIZE.
	// Индексация всегда начинается с 0, поэтому нижняя граница не задается.
	double array[ARRAY_SIZE];

	int count = 0;
	do
	{
		printf("Enter array size (up to %d): ", ARRAY_SIZE);
		scanf("%i", &count);
	}
	while (count < 0 && count >= ARRAY_SIZE);

	puts("Enter array elements: ");
	for (int i = 0; i < count; i++)
	{
		// Так можно вводить отдельные элементы массива.
		//
		// В С++ нет встроенной проверки выхода за границы массива:
		// если обратиться к array[ARRAY_SIZE], программа скомпилируется,
		// но ее поведение будет непредсказуемо (undefined behavior),
		// в частности, она может отработать якобы нормально.
		scanf("%lf", &array[i]);
	}

	// Используются сокращенные варианты цикла и условия без фигурных скобок.
	double mean = 0.0;
	for (int i = 0; i < count; i++)
		mean += array[i];
	if (count != 0)
		mean /= count;

	double standardDeviation = 0.0;
	for (int i = 0; i < count; i++)
	{
		double delta = array[i] - mean;
		standardDeviation += delta * delta;
	}
	if (count > 1)
		standardDeviation /= count - 1;

	printf("Approximate estimation (mean value): %g\n", mean);
	printf("Approximate variance (standard deviation): %g\n", standardDeviation);

	break_line();
}


// Демонстрация рекурсии и переполнения стека.
void present_recusion_and_stack_overflow()
{
	puts("Recursive power function demo.");
	puts("Try to find an exponent value big enough to produce stack overflow.");
	break_line();

	double base;
	printf("Enter the value of base (0 to return to main menu): ");
	scanf("%lf", &base);

	// Простейший бесконечный цикл (выход предполагается по переполнению стека).
	while (true)
	{
		int exponent;
		printf("Enter an exponent: ");
		scanf("%d", &exponent);

		// В этом месте происходит вызов рекурсивной функции.
		double result = recursive_power(base, exponent);

		printf("%g ** %+d = %g\n", base, exponent, result);
	}
}


// Выполняет перевод строки.
void break_line()
{
	putchar('\n');
}
