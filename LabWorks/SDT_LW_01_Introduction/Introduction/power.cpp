//	Вычисление целой неотрицательной степени.
//	Аргументы:
//		base		основание степени (дробное, неизменяемое);
//		exponent	показатель степени (неотрицательное целое, неименяемое).
double power(const double base, const unsigned int exponent)
{
	// В С и С++ есть только оператор возврата значения из функции
	// и нет аналога переменной Result в функциях языка Delphi,
	// поэтому, когда она нужна, ее требуется объявить вручную.
	double result = 1.0;

	for (unsigned i = 0; i < exponent; i++)
	{
		result *= base;
	}

	return result;
}

//	Вычисление целой степени.
//	Аргументы:
//		base		основание степени (дробное, неизменяемое);
//		exponent	показатель степени (целое, неименяемое).
double power(const double base, const int exponent)
{
	if (exponent < 0)
	{
		// Возврат результата рекурсивного вызова:
		//		x**y = (1/x)**(-y).
		return power(1.0 / base, -exponent);
	}
	else
	{
		double result = 1.0;
		for (int i = 0; i < exponent; i++)
		{
			result *= base;
		}
		return result;
	}
}

//	Вычисление степени рекурсивным способом.
//	Аргументы:
//		base		основание степени (дробное, неизменяемое);
//		exponent	показатель степени (целое, неименяемое).
double recursive_power(const double base, const int exponent)
{
	if (exponent < 0) 
	{
		return recursive_power(1.0 / base, -exponent);
	} 
	else if (exponent > 0) 
	{
		return base * recursive_power(base, exponent - 1);
	} 

	// Поскольку оператор return приводит к немедленному возврату управления
	// из функции, эта точка программы достигается только при exponent == 0.
	return 1.0;
}