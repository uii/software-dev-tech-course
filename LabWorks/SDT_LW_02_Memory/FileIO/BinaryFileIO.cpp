#include <cstdio>
#include <cstdint>

#include "BMP.h"


// Вычисляет модуль числа (absolute value).
int32_t abs(int32_t value)
{
	return value >= 0 ? value : -value;
}


void showBMPFileInfo()
{
	const size_t MAX_PATH = 256;
	char fileName[MAX_PATH];
	printf("Enter BMP image file name: ");
	gets(fileName);

	BitmapFileHeader fileHeader;
	DeviceIndependentBitmapHeader imageHeader;

	FILE *image = fopen(fileName, "rb");
	if (!image)
	{
		perror("Unable to open image file! ");
	}

	// Функция fread() читает данные из файла (последний аргумент)
	// и записывает их в область памяти по указателю (первый аргумент).
	// Размер данных -- N блоков (3-й аргумент) по M байт (второй аргумент),
	// всего N*M байт. Возвращается количество считанных полных блоков.
	fread(&fileHeader,	sizeof fileHeader,	1,	image);
	fread(&imageHeader,	sizeof imageHeader,	1,	image);

	fclose(image);

	// Первые два байта файла должны быть B и M.
	if (fileHeader.signature[0] != 'B' || fileHeader.signature[1] != 'M')
	{
		puts("This is not a BMP file.");
		return;
	}

	// Виды заголовков изображения отличаются по размеру,
	// здесь обрабатывается только самый популярный.
	if (imageHeader.headerSize != sizeof imageHeader)
	{
		printf(
			"Image header with size %u is unsupported!\n",
			imageHeader.headerSize);
		return;
	}

	// Высота изображения обычно записана в файл со знаком минус,
	// что означает: данные хранятся перевернутыми (нижний ряд -- первый).
	printf(
		"This is a %d x %d image with %hu bits per pixel (%s).\n",
		abs(imageHeader.width),
		abs(imageHeader.height),
		imageHeader.bitsPerPixel,
		imageHeader.bitsPerPixel <= 8 ? "has palette" : "no palette");

	// Для палитровых изображений размер данных нужно вычислять
	// (формула взята из документации на формат).
	size_t pixelDataSize = imageHeader.pixelDataSize;
	if (!pixelDataSize)
	{
		size_t rowSize =
			(imageHeader.bitsPerPixel * imageHeader.width + 31) / 4;
		pixelDataSize = rowSize * abs(imageHeader.height);
	}
	printf(
		"Image data starts at offset %u and takes %u bytes, %s.\n",
		fileHeader.pixelDataOffset,
		pixelDataSize,
		imageHeader.compressionMethod ? "compressed" : "uncompressed");
}
