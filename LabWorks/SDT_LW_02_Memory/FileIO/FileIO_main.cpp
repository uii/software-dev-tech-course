#include <cstdio>

void calculateStatisticsInFiles();
void showBMPFileInfo();

int main()
{
	const int CHOICE_TEXT = 1;
	const int CHOICE_BINARY = 2;

	int choice;
	do
	{
		puts  ("Available demos:");
		printf("\t[%d] Text file input & output\n", CHOICE_TEXT);
		printf("\t[%d] Binary file input\n", CHOICE_BINARY);
		puts  ("Type any other number to quit.");
		printf("What type of file I/O are you interested in? ");
		scanf ("%d", &choice);
		puts  ("");

		switch (choice)
		{
		case CHOICE_TEXT:
			calculateStatisticsInFiles();
			break;

		case CHOICE_BINARY:
			getchar();	// Чтобы удалить со стандартного ввода перевод строки.
			showBMPFileInfo();
			break;
		}

		puts("");
	}
	while (choice == CHOICE_TEXT || choice == CHOICE_BINARY);

    return 0;
}
