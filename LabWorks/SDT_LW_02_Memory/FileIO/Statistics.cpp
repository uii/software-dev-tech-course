#include "Statistics.h"

void calculateStatistics(
	float data[], size_t count, float *mean, float *standardDeviation)
{
	*mean = 0.0f;
	// Способ прохода по массиву: получить указатель на начало (0-й элемент)
	// и увеличивать его до тех пор, пока не получится адрес последнего.
	for (float *element = data; element != data + count; ++element)
	{
		*mean += *element;
	}
	if (count > 0)
	{
		*mean = *mean / count;
	}

	*standardDeviation = 0.0f;
	for (size_t i = 0; i < count; ++i)
	{
		float delta = data[i] - *mean;
		*standardDeviation += delta * delta;
	}
	if (count > 1)
	{
		*standardDeviation /= (count - 1);
	}
}
