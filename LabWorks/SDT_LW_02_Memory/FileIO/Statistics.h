#ifndef STATISTICS_H
#define STATISTICS_H

#include <cstddef>	// Для size_t.

// Вычисляет среднее значение и среднеквадратическое отклонение по выборке.
void calculateStatistics(
	// Входные данные:
	float data[],				// Данные выборки.
	size_t count,				// Размер выборки.
	// Выходные данные:
	float *mean,				// Указатель на выборочное среднее.
	float *standardDeviation	// Указатель на среднеквадратическое отклонение.
);

#endif
