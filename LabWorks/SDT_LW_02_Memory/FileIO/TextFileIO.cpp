#include <cstdio>

#include "Statistics.h"

void calculateStatisticsInFiles()
{
	// Тип FILE -- это дескриптор ("описатель") открытого файла,
	// подобный типу File или TextFile в Delphi. Второй аргумент
	// функции fopen() -- режим открытия файла (см. документацию).
	// Таким образом, здесь fopen() работает как пара функций
	// AssignFile() и Reset() в Delphi.
	FILE *input = fopen("files/SampleData.txt", "r");

	// При ошибке возвращается нулевой указатель.
	if (!input)
	{
		// Функция perror() печатает системное сообщение об ошибке,
		// предварив его переданной строкой (через двоеточие).
		perror("Unable to open input file! ");
		return;
	}

	const size_t MAX_DESCRIPTION_SIZE = 256;
	char sampleDescription[256] = { 0 };

	// Ввод строки из файла с указанием максимальной длины.
	fgets(sampleDescription, MAX_DESCRIPTION_SIZE, input);

	// Перевод строки уже записан в sampleDescription.
	printf("Sample description: %s", sampleDescription);

	// Чтение данных из текстового файла подобно чтению со стандартного ввода.
	// Более того, стандартный ввод -- это тоже файл, которому соответствует
	// глобальная переменная stdin, т. е. scanf(...) -- это fscanf(stdin, ...).
	size_t sampleSize;
	fscanf(input, "%u", &sampleSize);

	printf("Sample size: %u\n", sampleSize);

	float *sample = new float[sampleSize];

	for (size_t i = 0; i < sampleSize; ++i)
	{
		fscanf(input, "%f", &sample[i]);
	}

	// Закрывает файл (как CloseFile() в Delphi).
	fclose(input);

	float mean, standardDeviation;
	calculateStatistics(sample, sampleSize, &mean, &standardDeviation);

	delete [] sample;

	// Функция sprintf() выполняет форматный вывод в строку.
	char message[MAX_DESCRIPTION_SIZE];
	sprintf(
		message,
		"Mean value: %g\nStandard deviation: %g",
		mean, standardDeviation);
	puts(message);

	// Результаты расчетов будут записаны в файл (старое содержимое пропадает).
	FILE *output = fopen("files/Statistics.txt", "w");
	if (!output)
	{
		perror("Unable to open output file! ");
		return;
	}

	// Можно было бы использовать и fputs().
	fprintf(output, "%s", message);

	fclose(output);
}
