#include "LinkedList.h"
#include "Strings.h"

void appendString(LinkedList& list, const char *string)
{
	// Ключевое слово auto позволяет не указывать тип переменной --
	// он будет определен (выводен, deduced) компилятором.
	// Однако, следует применять его только в тех случаях,
	// когда человеку результат на важен или очевиден (сокращается запись).
	auto node = new ListNode();

	// В узле списка выделяется память под строку, и туда копируются
	// все символы. Обратите внимание на заботу о завершающем нуле (+1).
	node->string = new char[getStringLength(string) + 1];
	copyString(string, node->string);
	node->next = 0;

	if (list.head)
	{
		// Если в списке уже есть элементы (у пустого head == 0),
		// следующим элементом у последнего становится добавляемый,
		// и он же становится новым последним.
		list.head->next = node;
		list.head = node;
	}
	else
	{
		// Если в списке элементов не было, добавляемый узел
		// становится и "головой", и "хвостом" списка.
		list.tail = list.head = node;
	}
}


ListNode* findString(LinkedList& list, const char* string)
{
	// Пример удобства цикла for в языках С и C++:
	//	1) 	Переменная текущего узла при переборе (итератор)
	//		локальна в пределах цикла.
	//	2)	Условие node != 0 естественно становится условием работы цикла.
	for (ListNode *node = list.tail; node; node = node->next)
	{
		if (areStringsEqual(node->string, string))
		{
			return node;
		}
	}
	return 0;
}

// Ссылка (reference) действует как псевдоним для другой переменной.
// Здесь, например, параметр list имеет тип ссылки на структуру LinkedList;
// это означает, что list выступает псевдонимом для переменной, которая
// будет передана параметром функции, и при изменении полей list
// изменятся поля переданной переменной.
void resetList(LinkedList& list)
{
	list.head =
	list.tail = 0;
}

void destroyList(LinkedList& list)
{
	ListNode *current = list.tail;
	while (current)
	{
		// Перед удалением узла из памяти нужно сохранить указатель
		// на следующий элемент (после удаления это будет невозможно).
		ListNode *next = current->next;

		// Следует позаботиться об удалении строки, указатель на которую
		// хранится в узле, иначе случится утечка памяти (потеря указателя).
		delete [] current->string;

		delete current;

		current = next;
	}
	resetList(list);
}
