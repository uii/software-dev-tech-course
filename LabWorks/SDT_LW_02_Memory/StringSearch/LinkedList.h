#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED

/** \file LinkedList.h
	\brief Определения структур и функций для связанного списка строк. */


/** \brief Узел связанного списка. */
// Структуры обычно используются как записи (record) в Pascal и Delphi.
struct ListNode
{
	char *string;	/**< Хранимые данные (строка). */
	ListNode *next;	/**< Указатель на следующий элемент. */
};


/** \brief Связанный список (односвязный). */
struct LinkedList
{
	ListNode *head;	/**< \brief "Голова" (конец) списка: следующего элемента нет. */
	ListNode *tail;	/**< \brief "Хвост" (начало) списка. */
};


/**	\brief Добавляет в строку в список (создает новый узел и копирует туда строку).
	\param list		Связанный список.
	\param string	Строка, узел с которой будет добавлен. */
void appendString(LinkedList& list, const char *string);


/**	\brief Находит узел в связанном списке строк.
	\param list		Связанный список.
	\param string	Строка, узел с которой ищется.
	\return Узел, содержащий строку, равную заданной, или 0, если такого нет. */
ListNode* findString(LinkedList& list, const char* string);


/** \brief Сбрасывает указатели "головы" и "хвоста" списка в 0.
	\param list	Связанный список. */
void resetList(LinkedList& list);


/**	\brief Очищает список (удаляет узлы) и сбрасывает указатели.
	\param list Связанный список. */
void destroyList(LinkedList& list);

#endif // LINKEDLIST_H_INCLUDED
