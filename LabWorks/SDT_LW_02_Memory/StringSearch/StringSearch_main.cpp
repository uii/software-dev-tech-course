/*	Главный файл учебного проекта, демонстрирующего применение
	динамической памяти, указателей и ссылок для реализации
	связанного списка строк и поиска в нем. */

#include <cstdio>

#include "Strings.h"
#include "LinkedList.h"

// Проверяет правильность реализации функций для работы со строками C.
void testStringFunctions();

int main()
{
	testStringFunctions();

	LinkedList list;

	// Функция resetList() принимает ссылку, поэтому переменная list
	// может измениться после вызова (и изменится).
	resetList(list);

	puts("Enter strings; an empty line finishes input:");

	// Функция gets() считывает строку со стандартного ввода в буфер.
	char input[256];
	while (gets(input))
	{
		// Здесь массив передается как указатель. Дело в том, что имя массива
		// интерпретируется как указатель на его начальный (0-й) элемент.
		if (getStringLength(input) == 0)
		{
			break;
		}
		// Добавление строки в список.
		appendString(list, input);
	}

	puts("Enter empty line to stop searching strings.");
	do
	{
		printf("Enter the string you want to find: ");
		gets(input);
		if (!getStringLength(input))
		{
			break;
		}

		// Поиск строки в связанном списке.
		if (ListNode *node = findString(list, input))
		{
			// Несмотря на то, что обычно указатели -- простые целые числа,
			// печатать их значения следует только через "%p", так как
			// их размер может меняться при переходе 32 -- 64 бита и т. д.
			// Формат "%p" требует указателя void*, к которому приводим любой.
			printf("Found in list (node address: %p).\n", static_cast<void*>(node));
		}
		else
		{
			puts("Not found in list.");
		}
	}
	while (true);

	destroyList(list);

	return 0;
}
