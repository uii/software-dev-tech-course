#include <cassert>

#include "Strings.h"

void testStringFunctions()
{
	// Проверка правильности работы getStringLength().
	assert(getStringLength(0)			 ==	0);
	assert(getStringLength("")			 ==	0);
	assert(getStringLength("0123456789") ==	10);

	// Проверка правильности работы areStringsEqual().
	assert(areStringsEqual(0,			0)			== true);
	assert(areStringsEqual("",			"")			== true);
	assert(areStringsEqual(0,			"test")		== false);
	assert(areStringsEqual("example",	"example")	== true);
	assert(areStringsEqual("example",	"test")		== false);
	assert(areStringsEqual("test",		"example")	== false);

	const char *first = "This is a simple";
	const char *second = " test!";
	char result[256];

	// Проверка правильности работы concatenateStrings().
	concatenateStrings(first, second, result);
	assert(getStringLength(first) + getStringLength(second) == getStringLength(result));
	assert(areStringsEqual(result, "This is a simple test!"));
	concatenateStrings(first, "", result);
	assert(areStringsEqual(first, result));

	// Проверка правильности работы copyString().
	copyString(first, result);
	assert(areStringsEqual(first, result));
}
