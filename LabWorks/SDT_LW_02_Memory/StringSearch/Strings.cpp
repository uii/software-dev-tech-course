#include "Strings.h"

//	Вычисляет длину строки.
size_t getStringLength(const char* string)
{
	// Если указатель на входную строку нулевой,
	// строка считается пустой (длины 0).
	if (0 == string)
		return 0;

	size_t length = 0;	// Счетчик длины строки.

	// Пока очередной символ входной строки не нулевой,
	// увеличиваем счетчик длины. Можно было бы не делать сравнение,
	// а воспользоваться string[length] как условием.
	while (string[length] != '\0')
	{
		length++;
	}

	return length;
}


//	Проверяет строки на равенство.
bool areStringsEqual(const char *first, const char *second)
{
	// Если одна из строк - нулевой указатель, то строки считаются равными
	// тогда и только тогда, когда обе являются нулевыми указателями.
	if (0 == first || 0 == second)
	{
		return first == second;
	}

	// Проход по символам строк, пока не достигнут конец обеих.
	// Случай, когда конец одной из строк достигается раньше, обрабатывается
	// условием на равенство символов внутри цикла.
	for (size_t index = 0; first[index] != '\0' && second[index] != '\0'; index++)
	{
		// Сравниваем текущий символ в двух строках.
		if (first[index] != second[index])
		{
			return false; // Если символы не равны, строки не равны.
		}
	}

	return true;
}


//	Соединение двух строк в одну с использованием адресной арифметики.
void concatenateStrings(const char *first, const char *second, char *result)
{
	// Будем увеличивать first, пока он не будет указывать
	// на завершающий нулевой символ переданной строки.
	while (*first)
	{
		*result = *first;	// Очередной символ строки копируется в результат.
		first++;			// Переход к очередному символу первой строки.
		result++;			// Переход к очередному символу результата.
	}

	// Аналогично поступаем со второй строкой (в строку-результат уже
	// записана первая строка без '\0', а result указывает на следующий
	// символ в строке-результате после скопированной первой.
	while (*second)
	{
		*result = *second;
		second++;
		result++;
	}

	// В строку-результат необходимо дописать завершающий нуль.
	*result = '\0';
}


//	Копирует строку source в строку destination.
void copyString(const char* source, char* const destination)
{
	size_t index;
	for (index = 0; source[index] != '\0'; index++)
	{
		destination[index] = source[index];
	}
	destination[index] = '\0';
}
