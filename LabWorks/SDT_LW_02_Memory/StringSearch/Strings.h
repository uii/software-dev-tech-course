#ifndef STRINGS_H
#define STRINGS_H

#include <cstddef>	// Для size_t.

/** \file Strings.h
	\brief Функции для работы со строками С. */


/**	\brief Вычисляет длину строки (стандартная функция strlen).
	\param string Указатель на неизменяемые символы (сам указатель изменяемый).
				  Это и называется строкой C. */
size_t getStringLength(const char* string);


/**	\brief Проверяет строки на равенство (стандартная функция strcmp).
	\param first 	Первая строка.
	\param second	Вторая строка.
	\return Признак, что строки first и second совпадают. */
bool areStringsEqual(const char *first, const char *second);


/**	\brief Соединеняет две строки C в одну (стандартная функция strcat).

	\remarks Обратите внимание, что строка-результат представлена указателем
			 на изменяемые символы, туда будет вестись запись. Область памяти
			 для строки-результата должна быть достаточно велика, чтобы вместить
			 результат соединения строк (обычный контракт таких функций).

	\param [in]  first	Первая из соединяемых строк.
	\param [in]	 second	Вторая из соединяемых строк.
	\param [out] result	Строка-результат. */
void concatenateStrings(const char *first, const char *second, char *result);


/**	\brief Копирует строки C (стандартная функция strcpy).

	\remarks Обратите внимание на использование сonst для придания константности:
		а) только данным в случае source;
		б) только самому указателю (адресу) в случае destination;
	В принципе, можно было бы сделать и указатель source неизменяемым.

	\param [in]	 source 		Строка-источник.
	\param [out] destination	Строка-приемник. Должна указывать на область
								памяти достаточной длины, чтобы вместить
								все символы источника. */
void copyString(const char* source, char* const destination);

#endif
