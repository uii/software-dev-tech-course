//	DataStructures_main.cpp --
//		основной файл проекта DataStructures, содержащего эталонные реализации
//		вариантов заданий к ЛР №3 "ООП" по С++, относящихся к структурам данных.
//
//	Файлы с подрасширением .Tests содержат текты к соответствующему классу.
//	Файл Tests.h содержит определения всех тестирующих функций.

#include "Tests.h"

#include <cstdio>

int main()
{
	testDynamicArray();
	testList();
	testDeque();

    puts("All tests passed successfully!");

	return 0;
}
