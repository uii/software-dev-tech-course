#include <cassert>

#include "Deque.h"

static const double sequence[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

static void fillDeque(Deque& deque)
{
	for (int i = 4; i >= 0; i--)
	{
		deque.pushFront(sequence[i]);
	}
	for (size_t i = 5; i < 10; i++)
	{
		deque.pushBack(sequence[i]);
	}
}

static Deque getDeque(const Deque& source)
{
	return Deque(source);
}

void testDeque()
{
	Deque deque;
	assert(deque.isEmpty());

	deque.pushFront(0.0);
	assert(!deque.isEmpty());
	assert(deque.peekFront() == deque.peekBack());
	deque.popBack();
	assert(deque.isEmpty());

	fillDeque(deque);

	for (size_t i = 0; i < 5; i++)
	{
		assert(deque.popFront() == sequence[i]);
		assert(deque.popBack() == sequence[9 - i]);
	}
	assert(deque.isEmpty());

	fillDeque(deque);
	deque.clear();
	assert(deque.isEmpty());

	Deque empty;
	assert(deque == empty);

	fillDeque(deque);

	Deque copy(deque);
	assert(deque == copy);

	Deque copyAssigned = deque;
	assert(deque == copyAssigned);

	Deque moveAssigned = getDeque(deque);
	assert(moveAssigned == deque);

	assert(deque == getDeque(deque));
}