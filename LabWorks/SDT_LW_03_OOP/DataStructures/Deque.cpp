#include <cassert>

#include "Deque.h"

Deque::Deque()
{
	zero();
}

Deque::~Deque()
{
	clear(true);
}

Deque::Deque(const Deque& source)
{
	zero();
	copyFrom(source);
}

Deque::Deque(Deque&& source)
{
	moveFrom((Deque&&)source);
}

Deque& Deque::operator=(const Deque& rightSide)
{
	clear();
	copyFrom(rightSide);
	return *this;
}

Deque& Deque::operator=(Deque&& rightSide)
{
	clear(true);
	moveFrom((Deque&&)rightSide);
	return *this;
}

void Deque::copyFrom(const Deque& source)
{
	if (!source.isEmpty())
	{
		Node* node = source.front;
		while (node && node != source.back->next)
		{
			pushBack(node->data);
			node = node->next;
		}
	}
}

void Deque::moveFrom(Deque&& source)
{
	front = source.front;
	back = source.back;
	size = source.size;
	source.zero();
}

void Deque::clear(bool freeMemory)
{
	if (freeMemory)
	{
		while (front)
		{
			front = front->previous;
		}
		while (front)
		{
			Node *victim = front;
			front = front->next;
			delete victim;
		}
		back = 0;
	}
	else
	{
		front = back;
	}
	size = 0;
}

void Deque::zero()
{
	front = back = 0;
	size = 0;
}

const bool Deque::operator==(const Deque& other)
{
	if (other.isEmpty())
	{
		return this->isEmpty();
	}
	else if (size == other.size)
	{
		Node *thisNode = this->front;
		Node *otherNode = other.front;
		while (thisNode && thisNode != back->next)
		{
			if (thisNode->data != otherNode->data)
			{
				return false;
			}
			thisNode = thisNode->next;
			otherNode = otherNode->next;
		}
		return true;
	}
	return false;
}

const double Deque::popFront()
{
	assert(!isEmpty());
	const double result = front->data;
	if (front->next)
	{
		front = front->next;
	}
	size--;
	return result;
}

const double Deque::popBack()
{
	assert(!isEmpty());
	const double result = back->data;
	if (back->previous)
	{
		back = back->previous;
	}
	size--;

	return result;
}

void Deque::pushFront(const double item)
{
	if (isEmpty())
	{
		if (!front)
		{
			front = back = new Node();
			front->next = 0;
			front->previous = 0;
		}
	}
	else if (front->previous)
	{
		front = front->previous;
	}
	else 
	{
		Node *newFront = new Node();
		front->previous = newFront;
		newFront->previous = 0;
		newFront->next = front;
		front = newFront;
	}
	front->data = item;
	size++;
}

void Deque::pushBack(const double item)
{
	if (isEmpty())
	{
		if (!back)
		{
			back = front = new Node();
			back->next = 0;
			back->previous = 0;
		}
	}
	else if (back->next)
	{
		back = back->next;
	}
	else 
	{
		Node *newBack = new Node();
		back->next = newBack;
		newBack->next = 0;
		newBack->previous = back;
		back = newBack;
	}
	back->data = item;
	size++;
}

const double Deque::peekFront() const
{
	assert(!isEmpty());
	return front->data;
}

const double Deque::peekBack() const
{
	assert(!isEmpty());
	return back->data;
}