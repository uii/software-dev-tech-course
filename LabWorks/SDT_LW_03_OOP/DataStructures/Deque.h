#ifndef DEQUE_H
#define DEQUE_H

#include <cstddef>	// Для size_t.

class Deque 
{
public:
	Deque();
	Deque(const Deque& source);
	Deque(Deque&& source);
	virtual ~Deque();
public:
	// Помещает элемент в начало очереди.
	void pushFront(const double item);

	// Извлекает из начала очереди элемент и возвращает его.
	const double popFront();
	
	// Возвращает элемент в начале очереди, не извлекая его.
	const double peekFront() const;

	// Помещает элемент в конец очереди.
	void pushBack(const double item);

	// Извлекает с конца очереди элемент и возвращает его.
	const double popBack();
	
	// Возвращает элемент в конце очереди, не извлекая его.
	const double peekBack() const;

	// Возвращает признак, что очередь пуста.
	const bool isEmpty() const { return !size; }
	
	// Очищает очередь.
	void clear(const bool freeMemory = false);
public:
	Deque& operator=(const Deque& rightSide);
	Deque& operator=(Deque&& rightSide);  
	const bool operator==(const Deque& other);

private:
	struct Node
	{
		Node* previous;
		Node* next;
		double data;
	};
	Node *front;
	Node *back;
	size_t size;
private:
	void copyFrom(const Deque& source);
	void moveFrom(Deque&& source);
	void zero();
};

#endif