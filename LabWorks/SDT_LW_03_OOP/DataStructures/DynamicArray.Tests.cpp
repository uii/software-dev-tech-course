#include <cassert>

#include "DynamicArray.h"

static const double sequence[]		  = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
static const double sequenceWithout[] = { 1, 2, 3, 4, 5, 6,    8, 9, 10 };

static const double adder(const double item, const double accumulated)
{
	return accumulated + item;
}

static DynamicArray getDynamicArray()
{
	return DynamicArray(sequence, 10);
}

void testDynamicArray()
{
	DynamicArray array;
	
	for (size_t i = 5; i < 10; i++)
	{
		array.insertAt(i - 5, sequence[i]);
	}
	for (int i = 4; i >= 0; i--)
	{
		array.insertAt(0, sequence[i]);
	}

	for (size_t i = 0; i < 10; i++)
	{
		assert(array[i] == sequence[i]);
	}
	assert(array == sequence);

	array.removeAt(6);
	assert(array == sequenceWithout);

	array.insertAt(6, 7.0);
	assert(array == sequence);

	assert(array.find(7.0) == 6);
	assert(array.find(0) == -1);

	assert(array.foldLeft(adder) == 55);

	DynamicArray copy(array);
	assert(array == copy);

	DynamicArray assigned = array;
	assert(array == assigned);

	DynamicArray moveAssigned = getDynamicArray();
	assert(moveAssigned == array);

	assert(getDynamicArray() == array);
}