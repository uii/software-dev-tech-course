#include "DynamicArray.h"

#include <cassert>
#include <cstring>	// Для memcpy() и memmove().

DynamicArray::DynamicArray()
{
	zero();
}

DynamicArray::~DynamicArray()
{
	clear();
}

DynamicArray::DynamicArray(const double *data, const size_t count)
{
	zero();
	copyFrom(data, count);
}

DynamicArray::DynamicArray(const DynamicArray& copiedOne)
{
	zero();
	copyFrom(copiedOne);
}

DynamicArray::DynamicArray(DynamicArray&& movedOne)
{
	zero();
	moveFrom((DynamicArray&&)movedOne);
	movedOne.zero();
}

const double DynamicArray::operator[](const size_t index) const
{
	assert(index < size);
	return elements[index];
}

void DynamicArray::setAt(const size_t index, const double item)
{
	assert(index < size);
	elements[index] = item;
}

void DynamicArray::insertAt(const size_t index, const double item)
{
	assert(index <= size);
	if (size == capacity)
	{
		capacity += growthSize;
		double *newElements = new double[capacity];
		if (elements)
		{
			for (size_t i = 0; i < size; i++)
				newElements[i] = elements[i];
			delete [] elements;
		}
		elements = newElements;
	}
	memmove(elements + index + 1, elements + index, (size - index) * sizeof(double));
	elements[index] = item;
	size++;
}

void DynamicArray::removeAt(const size_t index)
{
	assert(index < size);
	memmove(elements + index, elements + index + 1, (size - index - 1) * sizeof(double));
	size--;
}

const int DynamicArray::find(const double item) const
{
	for (size_t i = 0; i < size; i++)
	{
		if (elements[i] == item)
		{
			return i;
		}
	}
	return -1;
}

void DynamicArray::clear()
{
	if (elements)
	{
		delete [] elements;
		elements = 0;
	}
	zero();
}

void DynamicArray::zero()
{
	elements = 0;
	size = 0;
	capacity = 0;
}

void DynamicArray::copyFrom(const DynamicArray& source)
{
	copyFrom(source.elements, source.size);
}

void DynamicArray::copyFrom(const double *data, const size_t count)
{
	size = count;
	if (capacity < count)
	{
		if (elements)
		{
			delete [] elements;
		}
		elements = new double[count];
		capacity = count;
	}
	memcpy(elements, data, count * sizeof(double));
}

void DynamicArray::moveFrom(DynamicArray&& source)
{
	this->capacity = source.capacity;
	this->size = source.size;
	this->elements = source.elements;
	source.zero();
}

DynamicArray& DynamicArray::operator=(const DynamicArray& rightSide)
{
	copyFrom(rightSide);
	return *this;
}

DynamicArray& DynamicArray::operator=(DynamicArray&& rightSide)
{
	clear();
	moveFrom((DynamicArray&&)rightSide);
	return *this;
}

const bool DynamicArray::operator==(const double *array) const
{
	bool isEqual = true;
	for (size_t i = 0; isEqual && i < size; i++)
	{
		isEqual &= elements[i] == array[i];
	}
	return isEqual;
}
const bool DynamicArray::operator==(const DynamicArray& other) const
{
	return other.size == this->size && other == this->elements;
}

const double DynamicArray::foldLeft(const double processor(const double, const double))
{
	double result = 0.0;
	for (size_t i = 0; i < size; i++)
	{
		result = processor(elements[i], result);
	}
	return result;
}