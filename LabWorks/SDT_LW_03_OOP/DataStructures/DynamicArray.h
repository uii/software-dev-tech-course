#ifndef DYNAMIC_ARRAY_H
#define DYNAMIC_ARRAY_H

#include <cstddef>	// Для size_t.

class DynamicArray
{
public:
	DynamicArray();
	DynamicArray(const double *data, const size_t count);
	DynamicArray(const DynamicArray& copiedOne);
	DynamicArray(DynamicArray&& movedOne);
	virtual ~DynamicArray();
public:
	// Возвращает текущую длину массива.
	const size_t getLength() const { return size; }

	// Возвращает элемент по заданному индексу.
	const double operator[](const size_t index) const;

	// Устанавливает значение элемента по заданному индексу.
	void setAt(const size_t index, const double item);

	// Вставляет элемент на место, заданное индексом.
	void insertAt(const size_t index, const double item);

	// Удаляет элемент с заданным индексом.
	void removeAt(const size_t index);

	// Возвращает индекс первого элемента, равного item, или -1.
	const int find(const double item) const;

	// Вычисляет левоассоциативную свертку списка (см. ниже).
	// Первый функции свертки аргумент -- элемент, второй --
	// её предыдущее значнеие при переборе списка слева направо.
	const double foldLeft(const double processor(const double, const double));
public:
	DynamicArray& operator=(const DynamicArray& rightSide);
	DynamicArray& operator=(DynamicArray&& rightSide);
	const bool operator==(const double *array) const;
	const bool operator==(const DynamicArray& other) const;
private:
	void zero();
	void clear();
	void copyFrom(const DynamicArray& source);
	void copyFrom(const double *data, const size_t count);
	void moveFrom(DynamicArray&& source);
private:
	double *elements;
	size_t capacity;
	size_t size;
private:
	const static size_t growthSize = 16;
};

#endif