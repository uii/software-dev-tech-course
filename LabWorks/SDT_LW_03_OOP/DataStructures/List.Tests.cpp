#include <cassert>
#include "List.h"

static const double sequence[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

static List getList()
{
	List list;
	for (size_t i = 0; i < 10; i++)
	{
		list.append(sequence[i]);
	}
	return list;
}

void testList()
{
	List list;
	for (size_t i = 0; i < 5; i++)
	{
		list.prepend(sequence[i]);
	}
	assert(list.getLength() == 5);

	list.remove(0.0);
	assert(list.getLength() == 4);

	list.remove(3.0);
	assert(list.getLength() == 3);

	list.remove(4.0);
	assert(list.getLength() == 2);

	list.append(4.0);
	assert(list.getLength() == 3);

	list.clear();
	assert(list.getLength() == 0);

	List::Enumerator enumerator = list.getEnumerator();
	assert(!enumerator.canAdvance());
              
	// I can't always advance(), but when I canAdvance(), I advance().
	const double *currentInsequence = sequence; 
	for (List::Enumerator i = list.getEnumerator(); i.canAdvance(); i.advance())
	{
		assert(i.getCurrent() == *currentInsequence++);
	}

	List copy(list);
	assert(copy == list);

	List copyAssigned = list;
	assert(copyAssigned == list);

	List moveAssigned = getList();
	assert(moveAssigned == list);

	assert(getList() == list);
}