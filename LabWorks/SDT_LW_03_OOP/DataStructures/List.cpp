#include <cassert>

#include "List.h"

List::List()
{
	zero();
}

List::List(const List& source)
{
	zero();
	copyFrom(source);
}

List::List(List&& source)
{
	zero();
	moveFrom((List&&)source);
}

List::~List()
{
	clear();
}

void List::copyFrom(const List& source)
{
	if (source.head)
	{
		head = new Node();
		tail = head;
		Node *current = source.head;
		while (current && current->next)
		{
			tail->data = current->data;
			tail->next = new Node();
			tail = tail->next;
			current = current->next;
		}
		tail->data = current->data;
		tail->next = 0;
	}
}

void List::moveFrom(List&& source)
{
	head = source.head;
	tail = source.tail;
	length = source.length;
	source.zero();
}

void List::zero()
{
	length = 0;
	head = 0;
	tail = 0;
}

void List::append(const double element)
{
	Node *node = new Node();
	node->data = element;
	node->next = 0;
	if (tail)
	{
		tail->next = node;
		tail = node;
	}
	else
	{
		head = node;
		tail = node;
	}
	length++;
}

void List::prepend(const double element)
{
	Node *node= new Node();
	node->data = element;
	node->next = head;
	head = node;
	if (!tail)
	{
		tail = node;
	}
	length++;
}

void List::remove(const double element)
{
	if (head)
	{
		Node *current = head, *previous = 0, *victim = 0;
		while (current)
		{
			if (current->data == element)
			{
				victim = current;
				if (victim == head)
				{
					head = current->next;
					current = current->next;
				}
				else if (victim == tail)
				{
					tail = previous;
					previous->next = 0;
					current = 0;
				}
				else
				{
					previous->next = current->next;
					current = current->next;
				}
				delete victim;
				length--;
			}
			else
			{
				previous = current;
				current = current->next;
			}
		}
	}
}

void List::clear()
{
	while (head)
	{
		Node *next = head->next;
		delete head;
		head = next;
	}
	tail = 0;
	length = 0;
}

List List::operator=(const List& rightSide)
{
	clear();
	copyFrom(rightSide);
	return *this;
}

List List::operator=(List&& rightSide)
{
	clear();
	moveFrom((List&&)rightSide);
	return *this;
}

const bool List::operator==(const List& rightSide)
{
	if (length == rightSide.length)
	{
		Enumerator thisEnumerator = getEnumerator();
		Enumerator otherEnumerator = rightSide.getEnumerator();
		while (thisEnumerator.canAdvance() && otherEnumerator.canAdvance())
		{
			if (thisEnumerator.getCurrent() != otherEnumerator.getCurrent())
			{
				return false;
			}
		}
		return true;
	}
	return true;
}

void List::Enumerator::advance()
{
	assert(canAdvance());
	current = current->next;
}