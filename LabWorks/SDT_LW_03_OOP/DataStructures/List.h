#ifndef LIST_H
#define LIST_H

#include <cstddef>	// Для size_t.

// Связанный список с добавленеим в конец и классом-переборщиком.
class List
{
public:
	List();
	List(const List& source);
	List(List&& source);
	virtual ~List();
public:
	// Возвращает текущую длину списка.
	const size_t getLength() const { return length; }

	// Добавляет элемент с указанными данными в конец списка.
	void append(const double element);

	// Добавляет элемент в начало списка.
	void prepend(const double element);

	// Удаляет все элементы с указанными данными из списка.
	void remove(const double element);

	// Очищает список.
	void clear();
public:
	List operator=(const List& rightSide);
	List operator=(List&& rightSide);
	const bool operator==(const List& rightSide);

private:
	struct Node
	{
		double data;
		Node *next;
	};
private:
	void copyFrom(const List& source);
	void moveFrom(List&& source);
	void zero();
private:
	size_t length;
	Node *head, *tail;

public:
	class Enumerator
	{
	public:
		Enumerator(Node *start) : current(start) { }
	public:
		const bool canAdvance() const { return !!current; }
		void advance();
		const double getCurrent() const { return current->data; }
	private:
		struct Node *current;
	};

public:
	// Создает объект для перебора списка.
	Enumerator getEnumerator() const { return Enumerator(head); }
};

#endif