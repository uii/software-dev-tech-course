#ifndef EXPRESSION_H
#define EXPRESSION_H

// Интерфейс (класс, включающий только чисто виртуальные методы) выражения.
class IExpression
{
public:
	// Вычисляет значение выражения.
	virtual double evaluate() const = 0;
};

#endif