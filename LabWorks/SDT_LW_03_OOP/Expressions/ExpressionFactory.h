#ifndef EXPRESSION_FACTORY_H
#define EXPRESSION_FACTORY_H

#include "../Strings/String.h"

// Предварительное объявление интерфейса IExpression.
class IExpression;


// Интерфейс абстрактной фабрики выражений (abstract factory), 
// то есть создающий объекты конкретных классов выражений, имеющих интерфейс IExpression.
class IExpressionFactory
{
public:
	// Создает выражение, представляющее число-параметр.
	virtual IExpression* createNumber(double number) const = 0;

	// Создает выражение отрицания (минус) аргумента.
	virtual IExpression* createNegation(IExpression *expression) const = 0;
	
	// Создает выражение сложения выражений-аргументов.
	virtual IExpression* createAddition(IExpression *left, IExpression *right) const = 0;
	
	// Создает выражение умножения выражений-аргументов.
	virtual IExpression* createMultiplication(IExpression *left, IExpression *right) const = 0;

	// Создает выражение деления выражений-аргументов.
	virtual IExpression* createDivision(IExpression *left, IExpression *right) const = 0;
	
	// Создает выражение возведения арагумента-основания в аргумент-степень.
	virtual IExpression* createPower(IExpression *base, IExpression *exponent) const = 0;
};

#endif