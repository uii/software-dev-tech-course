#include "ExpressionParser.h"

#include <cstdio>	// Для sscanf().
#include <cassert>
#include <cctype>	// Для isdigit(), isspace().

ExpressionParser::Lexer::Lexer(const char *text)
	: expression(text)
{
	stream = expression;
	lastTokenLength = 0;
	lastTokenType = TokenType::Unknown;
	advance();
}

void ExpressionParser::Lexer::advance()
{
	if (!canAdvance())
	{
		lastTokenLength = 0;
		lastTokenType = TokenType::Unknown;
		return;
	}

	stream += lastTokenLength;

	skipWhiteSpace();
	lastTokenLength = 1;
	switch (*stream)
	{
	case '+':
		lastTokenType = TokenType::Plus;
		break;
	case '-':
		lastTokenType = TokenType::Minus;
		break;
	case '*':
		lastTokenType = TokenType::Asterisk;
		break;
	case '/':
		lastTokenType = TokenType::Slash;
		break;
	case '^':
		lastTokenType = TokenType::Caret;
		break;
	case '(':
		lastTokenType = TokenType::OpeningBrace;
		break;
	case ')':
		lastTokenType = TokenType::ClosingBrace;
		break;
	default:
		lastTokenType = TokenType::Unknown;
		if (isdigit(*stream))
		{
			double number;
			int charactersRead;
			int fieldsRead = sscanf(stream, "%lf%n", &number, &charactersRead);
			if (fieldsRead == 1)
			{
				lastTokenType = TokenType::Number;
				lastTokenLength = charactersRead;
			}
		}
		else if (*stream == '\0')
		{
			lastTokenType = TokenType::Nothing;
			lastTokenLength = 0;
		}
		break;
	}
}

void ExpressionParser::Lexer::skipWhiteSpace()
{
	while (*stream && isspace(*stream))
	{
		stream++;
	}
}

ExpressionParser::ExpressionParser(const IExpressionFactory *expressionFactory)
{
	assert(expressionFactory != 0);
	this->expressionFactory = expressionFactory;
	errorPosition = -1;
}

IExpression* ExpressionParser::parse(const String& text)
{
	Lexer lexer(text.getData());
	this->lexer = &lexer;
	errorPosition = -1;
	errorMessage.clear();
	IExpression *result = parseAddition();
	this->lexer = 0;
	return result;
}

IExpression* ExpressionParser::parseAddition()
{
	if (IExpression *left = parseMultiplication())
	{
		TokenType operation = lexer->getLastTokenType();
		while (operation == TokenType::Plus || operation == TokenType::Minus)
		{
			lexer->advance();
			if (IExpression *right = parseMultiplication())
			{
				if (operation == TokenType::Minus)
				{
					right = expressionFactory->createNegation(right);
				}
				left = expressionFactory->createAddition(left, right);
			}
			else
			{
				return 0;
			}
			operation = lexer->getLastTokenType();
		}
		return left;
	}
	return 0;
}

IExpression* ExpressionParser::parseMultiplication()
{
	if (IExpression *left = parseUnary())
	{
		TokenType operation = lexer->getLastTokenType();
		while (operation == TokenType::Asterisk || operation == TokenType::Slash)
		{
			lexer->advance();
			if (IExpression *right = parseUnary())
			{
				switch (operation)
				{
				case TokenType::Asterisk:
					left = expressionFactory->createMultiplication(left, right);
					break;
				case TokenType::Slash:
					left = expressionFactory->createDivision(left, right);
					break;
				default:
					setError("Internal expression parsing error.");
					return 0;
				}
			}
			else
			{
				return 0;
			}
			operation = lexer->getLastTokenType();
		}
		return left;
	}
	return 0;
}

IExpression* ExpressionParser::parsePower()
{
	if (IExpression *base = parseAtom())
	{
		if (lexer->getLastTokenType() == TokenType::Caret)
		{
			lexer->advance();
			if (IExpression *exponent = parseUnary())
			{
				return expressionFactory->createPower(base, exponent);
			}
			return 0;
		}
		return base;
	}
	return 0;
}

IExpression* ExpressionParser::parseUnary()
{
	bool isSigned = false;
	bool mustInvert = false;
	do
	{
		bool isNegation = lexer->getLastTokenType() == TokenType::Minus;
		isSigned = isNegation || lexer->getLastTokenType() == TokenType::Plus;
		mustInvert = mustInvert ^ isNegation;
		if (isSigned)
		{
			lexer->advance();
		}
	} while (isSigned);

	if (IExpression* innerExpression = parsePower())
	{
		if (mustInvert)
		{
			return expressionFactory->createNegation(innerExpression);
		}
		return innerExpression;
	}
	return 0;
}

IExpression* ExpressionParser::parseAtom()
{
	switch (lexer->getLastTokenType())
	{
	case TokenType::OpeningBrace:
		lexer->advance();
		if (IExpression* innerExpression = parseAddition())
		{
			if (lexer->getLastTokenType() == TokenType::ClosingBrace)
			{
				lexer->advance();
				return innerExpression;
			}
			else
			{
				setError("Closing brace expected.");
			}
		}
		return 0;

	case TokenType::Number:
		double number;
		sscanf(lexer->getLastToken().getData(), "%lf", &number);
		lexer->advance();
		return expressionFactory->createNumber(number);

	default:
		setError("Opening brace or number expected.");
	}
	return 0;
}

void ExpressionParser::setError(const char *message)
{
	errorMessage = message;
	errorPosition = lexer->getPosition();
}
