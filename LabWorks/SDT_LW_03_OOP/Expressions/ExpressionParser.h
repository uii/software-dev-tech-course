#ifndef EXPRESSION_PARSER_H
#define EXPRESSION_PARSER_H

#include "../Strings/String.h"
#include "ExpressionFactory.h"

/*	
	Класс для разбора (parsing) арифметических выражений (разг. "парсер").
	
	Обратите внимание на архитектурное решение: парсер "не знает" ничего о том,
	какие выражения он порождает, а только как они связаны. Это возможно, благодаря 
	применению абстрактной фабрики -- известного шаблона проектирования (design pattern). 
	
	Для использования необходимо знать только его интерфейс (преимущество абстракции!).
	Способ разбора выражения -- т. н. рекурсивный спуск (recursive descent).
	Аналогичный код подробно разбирается в книге Г. Шилдта "Полный справочник по С++".
*/
class ExpressionParser
{
public:
	// Создает объект парсера с заданной фабрикой выражений.
	ExpressionParser(const IExpressionFactory *expressionFactory);

	// Выполняет разбор строки выражения и возвращает выражение. При ошибке возвращает 0.
	IExpression* parse(const String& text);

	// Возвращает место ошибки или (-1), если ошибок нет.
	const int getErrorPosition() const { return errorPosition; }

	// Возвращает описанеи ошибки или 0, если ошибок нет.
	const char* getErrorMessage() const { return errorMessage.getData(); }

private:
	/*	Перечисление (enumeration) подобно перечислимому типу в Delphi.
		C++11 введены строго типизированные перечисления (strongly-typed enums)
		со словом class: в этом случае обращение к элементу перечисления
		оформляются как TokenType::Plus, а не просто Plus, а кроме того,
		тип этого перечисления не совместим ни с какими другими перечислениями.
		
		Перечисление задает типы лексем (см. ниже).	
	*/
	enum class TokenType 
	{
		Nothing,		//	Ничего (в конец выражения).
		Plus,			//	+
		Minus,			//	-
		Asterisk,		//	*
		Slash,			//	/	
		Caret,			//	^
		OpeningBrace,	//	(
		ClosingBrace,	//	)
		Number,			//	Число (целое или дробное).
		Unknown			//	Нераспознанная лексема.
	};

	/*	Лексический анализатор (разг. "лексер") предназначен для преобразования
		строки выражения в неделимые элементы -- лексемы (tokens).
		Примеры лексем: 123, +, (. Данный лексер предоставляет доступ только
		к последней распознанной лексеме, но бывают и более сложные анализаторы. 
	*/
	class Lexer 
	{
		const char* expression;
		const char *stream;
		TokenType lastTokenType;
		size_t lastTokenLength;
		void skipWhiteSpace();
	public:
		Lexer(const char *text);
		void advance();
		const bool canAdvance() const { return *stream != '\0'; }
		const size_t getPosition() const { return (stream - expression) + 1; }
		const TokenType getLastTokenType() const { return lastTokenType; }
		const String getLastToken() const { return String(stream, lastTokenLength); }
	};

	const IExpressionFactory *expressionFactory;
	int errorPosition;
	String errorMessage;
	Lexer *lexer;

	void setError(const char* message);
	IExpression* parseAtom();
	IExpression* parseUnary();
	IExpression* parsePower();
	IExpression* parseMultiplication();
	IExpression* parseAddition();
};

#endif