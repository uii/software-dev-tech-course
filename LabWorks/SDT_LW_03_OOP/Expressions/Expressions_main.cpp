#include "ExpressionParser.h"

// TODO: исключить в финальной версии.
#include "SampleSolution.h"

#include <cfloat>	// Для DBL_EPSILON.
#include <cmath>	// Для std::abs().
#include <cstdio>

bool checkEvaluation(ExpressionParser& parser, const char* expression, const double expected)
{
	// Предельная погрешность вычисления выражения.
	static const double ACCURACY_THRESHOLD = 16 * DBL_EPSILON;

	IExpression *parsed = parser.parse(expression);
	if (parsed)
	{
		double evaluated = parsed->evaluate();
		if (std::abs(evaluated - expected) < ACCURACY_THRESHOLD)
		{
			printf("%s = %g\n", expression, evaluated);
			return true;
		}
		else
		{
			printf(
				"Invalid result calculating \"%s\"! Expected: %g, got: %g.\n",
				expression, expected, evaluated);
		}
	}
	else
	{
		printf(
			"Unable to parse \"%s\" at symbol %u - %s\n",
			expression, parser.getErrorPosition(), parser.getErrorMessage());
	}
	return false;
}

int main()
{
	// TODO: в финальной версии expressionFactory = 0 изначально.
	ExpressionFactory sampleExpressionFactory;
	IExpressionFactory *expressionFactory = &sampleExpressionFactory;
	ExpressionParser parser(expressionFactory);

	// Объявление типа (анонимного, сиречь, безымянного),
	// совмещенное с объявлением переменной tests
	// и ее инициализацией списком (initializer list).
	struct
	{
		const char *expression; // Выражение.
		double expected;		// Ожидаемое значение.
	}
	tests[] =
	{
		{ "0",							 0		},
		{ "1 + 2",						+3		},
		{ "2 * 3",						+6		},
		{ "3 ^ 4",						+81		},
		{ "-+--5",						-5		},
		{ "(6)",						+6		},
		{ "7 / 7",						+1		},
		{ "1 + 2 - 3 + 4 - 5",			-1		},
		{ "1 * 2 * 3 * 4 / 8",			+3		},
		{ "4^3^2^1",					+262144	},
		{ "-(1 - 2)*(3 / 4) / 9^0.5",	+0.25	}
	};

	// C++11 вводит новый вид цикла for для диапазонов (range-based for loop).
	// До двоеточия в скобках объявляется переменная-переборщик (iterator),
	// а после скобок находится сущность, поддерживающий разыменование и инкремент
	// (это может быть объект, а может быть указатель, как в данном случае).
	//
	// Ключевое слово auto позволяет не указывать тип переменной, в этом случае
	// компилятор попытается вывести его автоматически (при невозможности будет ошибка).
	//
	for (auto test : tests)
	{
		if (!checkEvaluation(parser, test.expression, test.expected))
        {
            puts("Test failed, aborting.");
            return 0;
        }
	}

	assert(!parser.parse(""));
	assert(!parser.parse("1 ++ /4"));
	assert(!parser.parse("one plus two"));

	puts("All tests passed successfully!");

	return 0;
}
