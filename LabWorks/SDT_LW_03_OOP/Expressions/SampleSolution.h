/*	
	Это пример, неполный и плохой, решения варианта задания:
		* память течет рекой;
		* архитектура просит Visitor;
		* имена не везде удачные.
	Данный код никогда не будет использован в обучении, он отладочный.

														Дмитрий Козлюк
 */
#ifndef SAMPLE_SOLUTION_H
#define SAMPLE_SOLUTION_H

#include "ExpressionFactory.h"
#include "Expression.h"

#include <cassert>
#include <cfloat>
#include <cmath>

class Expression : public IExpression
{
public:
	virtual ~Expression() { }
};

class NumberExpression final : public Expression
{
private:
	const double number;
public:
	NumberExpression(const double number) : number(number) { }
	virtual double evaluate() const override { return number; }
};

class BinaryExpression : public Expression
{
protected:
	IExpression* left;
	IExpression* right;
public:
	BinaryExpression(IExpression* left, IExpression* right)
		: left(left), right(right) { }
};

class AdditionExpression final : public BinaryExpression
{
public:
	AdditionExpression(IExpression* left, IExpression* right)
		: BinaryExpression(left, right) { }

	virtual double evaluate() const override
	{
		return left->evaluate() + right->evaluate();
	}
};

class MultiplicationExpression final : public BinaryExpression
{
public:
	MultiplicationExpression(IExpression* left, IExpression* right)
		: BinaryExpression(left, right) { }

	virtual double evaluate() const override
	{
		return left->evaluate() * right->evaluate();
	}
};

class DivisionExpression final : public BinaryExpression
{
public:
	DivisionExpression(IExpression* left, IExpression* right)
		: BinaryExpression(left, right) { }

	virtual double evaluate() const override
	{
		double divisor = right->evaluate();
		assert(std::abs(divisor) >= DBL_EPSILON);
		return left->evaluate() / divisor;
	}
};

class PowerExpression final: public BinaryExpression
{
public:
	PowerExpression(IExpression* left, IExpression* right)
		: BinaryExpression(left, right) { }

	virtual double evaluate() const override
	{
		double base = left->evaluate();
		double exponent = right->evaluate();
		assert(base >= 0 || std::abs(exponent) - floor(std::abs(exponent)) <= DBL_EPSILON);
		return pow(base, exponent);
	}
};

class NegationExpression final: public Expression
{
private:
	IExpression *innerExpression;
public:
	NegationExpression(IExpression* innerExpression)
		: innerExpression(innerExpression)
	{ }

	virtual double evaluate() const override
	{
		return -innerExpression->evaluate();
	}
};

class ExpressionFactory: public IExpressionFactory
{
public:
	virtual IExpression* createNumber(double number) const 
	{
		return new NumberExpression(number);
	}

	virtual IExpression* createNegation(IExpression *expression) const 
	{
		return new NegationExpression(expression);
	}
	
	virtual IExpression* createAddition(IExpression *left, IExpression *right) const 
	{
		return new AdditionExpression(left, right);
	}
	
	virtual IExpression* createMultiplication(IExpression *left, IExpression *right) const 
	{
		return new MultiplicationExpression(left, right);
	}

	virtual IExpression* createDivision(IExpression *left, IExpression *right) const 
	{
		return new DivisionExpression(left, right);
	}
	
	virtual IExpression* createPower(IExpression *base, IExpression *exponent) const 
	{
		return new PowerExpression(base, exponent);
	}
};

#endif