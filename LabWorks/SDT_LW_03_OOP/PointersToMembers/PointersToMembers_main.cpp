﻿#include <cstdio>

class IClickDelegate
{
public:
	virtual void invoke() = 0;
};

class Window;

class WindowClickDelegate : public IClickDelegate
{
public:
	typedef void (Window::*ButtonClickEventHandler)(void);
	WindowClickDelegate(Window& window, ButtonClickEventHandler handler)
		: window(window), handler(handler) { }
	virtual void invoke()
	{
		(window.*handler)();
	}
private:
	Window& window;
	ButtonClickEventHandler handler;
};

class Button
{
public:
	Button() :clickEvent(0) { }
	virtual ~Button() 
	{
		if (clickEvent)
		{
			delete clickEvent;
		}
	}
	void click()
	{
		if (clickEvent)
		{
			clickEvent->invoke();
		}
	}
	void setClickEvent(IClickDelegate *eventHandler)
	{
		clickEvent = eventHandler;
	}
private:
	IClickDelegate *clickEvent;
};

class Window
{
public:
	Window() : button()
	{
		button.setClickEvent(new WindowClickDelegate(*this, &Window::onButtonClicked));
	}
	void onButtonClicked()
	{
		puts("Button clicked!");
	}
	Button button;
};

int main()
{
	Window window;
	window.button.click();
	getchar();
	return 0;
}