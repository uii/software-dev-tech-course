#include "String.h"

#include <cstring>	// Для strncmp, strncpy, strcpy.

int AdvancedString::indexOf(const String& substring) const
{
	const int notFoundIndex = -1;
	size_t substringLength = substring.getLength();
	if (length < substringLength)
	{
		return notFoundIndex;
	}

	for (size_t i = 0; i <= length - substringLength; i++)
	{
		if (!strncmp(data + i, substring.getData(), substringLength))
		{
			return i;
		}
	}
	return notFoundIndex;
}

void AdvancedString::replaceFirst(const String& what, const String& with)
{
	int indexOfWhat = indexOf(what);
	if (indexOfWhat >= 0)
	{
		size_t newLength = length - what.getLength() + with.getLength();
		char *newData = new char[length + 1];
		strncpy(newData, data, indexOfWhat);
		strncpy(newData + indexOfWhat, with.getData(), with.getLength());
		strcpy(newData + indexOfWhat + with.getLength(), data + indexOfWhat + what.getLength());
		clear();
		data = newData;
		length = newLength;
	}
}
