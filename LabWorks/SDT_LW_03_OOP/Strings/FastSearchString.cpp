#include "String.h"
#include <cstring>

void FastSearchString::zero()
{
	AdvancedString::zero();
	this->failureFunction = 0;
}

void FastSearchString::copyFrom(const FastSearchString& source)
{
	setData(source.data);
	clearFailureFunction();
	failureFunction = new int[length];
	memcpy(failureFunction, source.failureFunction, sizeof(int) * length);
}

void FastSearchString::moveFrom(FastSearchString&& source)
{
	this->data = source.data;
	this->length = source.length;
	this->failureFunction = source.failureFunction;
	source.zero();
}

FastSearchString& FastSearchString::operator=(const FastSearchString& source)
{
	copyFrom(source);
	return *this;
}

FastSearchString& FastSearchString::operator=(FastSearchString&& source)
{
	clear();
	clearFailureFunction();
	moveFrom((FastSearchString&&)source);
	return *this;
}

FastSearchString::FastSearchString(const char *data) : AdvancedString(data), failureFunction(0)
{
	rebuildFailureFunction();
}

FastSearchString::FastSearchString(const FastSearchString& copiedOne)
{
	copyFrom(copiedOne);
}

FastSearchString::FastSearchString(FastSearchString&& movedOne) : AdvancedString(movedOne)
{
	zero();
	moveFrom((FastSearchString&&)movedOne);
}

FastSearchString::~FastSearchString()
{
	clearFailureFunction();
}

void FastSearchString::clearFailureFunction()
{
	if (failureFunction)
	{
		delete[] failureFunction;
	}
	failureFunction = 0;
}

void FastSearchString::rebuildFailureFunction()
{
	clearFailureFunction();
	failureFunction = new int[length];
	int candidate = 0;
	failureFunction[0] = -1;
	failureFunction[1] =  0;
	for (size_t index = 2; index < length; index++)
	{
		if (data[index - 1] == data[candidate]) 
		{
			candidate++;
			failureFunction[index] = candidate;
		}
		else if (candidate > 0)
		{
			candidate = failureFunction[candidate];
		}
		else
		{
			failureFunction[index] = 0;
		}
	}
}

int FastSearchString::indexOf(const String& substring) const
{
	size_t matchStart = 0;
	size_t index = 0;
	while (matchStart + index < length)
	{
		if (substring[index] == data[matchStart + index])
		{
			if (index == substring.getLength() - 1)
			{
				return matchStart;
			}
			index++;
		}
		else
		{
			matchStart += index - failureFunction[index];
			index = failureFunction[index] > -1 ? failureFunction[index] : 0;
		}
	}
	return -1;
}