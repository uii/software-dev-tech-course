#include "String.h"

#include <cstring>	// Для strcpy(), strcmp(), strncpy().

void String::zero()
{
	data = 0;
	length = 0;
}

bool operator==(const String& object, const char *sequence)
{
	size_t sequenceLength = strlen(sequence);
	if (object.getLength() == sequenceLength)
	{
		return !strcmp(object.getData(), sequence);
	}
	return false;
}

bool operator==(const char *sequence, const String& object)
{
	return object == sequence;
}

String::operator bool () const 
{
	return data && length;
}

const String operator+(const String& left, const String& right)
{
	String result(left);
	result.concatenateWith(right.data, right.length);
	return result;
}

String::String(const char *data): data(0), length(0)
{
	setData(data);
}

String::String(const char *data, const size_t length)
{
	this->length = length;
	this->data = new char[length + 1];
	strncpy(this->data, data, length);
	this->data[length] = '\0';
}

String::String(const String& copiedOne)
{
	zero();
	setData(copiedOne.data);
}

String::String(String&& movedOne) 
{
	moveFrom((String&&)movedOne);
}

void String::setData(const char *data)
{
	clear();
	this->length = strlen(data);
	this->data = new char[this->length + 1];
	strcpy(this->data, data);
}

void String::clear()
{
	if (data)
	{
		delete[] data;
	}
	zero();
}

String::~String()
{
	clear();
}

void String::moveFrom(String&& movedOne)
{
	data = movedOne.data;
	length = movedOne.length;
	movedOne.zero();
}

String& String::operator=(const String& rightSide)
{
	setData(rightSide.data);
	return *this;
}

String& String::operator=(String&& rightSide)
{
	clear();
	moveFrom((String&&)rightSide);
	return *this;
}

const String& String::operator+=(const String& rightSide)
{
	concatenateWith(rightSide.data, rightSide.length);
	return *this;
}

const String& String::operator+=(const char *rightSide)
{
	concatenateWith(rightSide, strlen(rightSide));
	return *this;
}

bool String::operator==(const String& other) const
{
	return (*this) == other.getData();
}

void String::concatenateWith(const char *rightSide, size_t rightSideLength)
{
	size_t newLength = length + rightSideLength;
	char *newData = new char[newLength + 1];
	strncpy(newData, data, length);
	strcpy(newData + length, rightSide);
	clear();
	data = newData;
	length = newLength;
}

const char String::operator[](size_t index) const
{
	if (index >= length) 
	{
		return 0;
	}
	return data[index];
}