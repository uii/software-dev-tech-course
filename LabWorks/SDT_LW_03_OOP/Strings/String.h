#ifndef STRING_H
#define STRING_H

// Microsoft предлагает использовать собственные версии функций работы со строками,
// использующие дополнительные проверки. Разумных причин так делать нет.
// Данный макрос, будучи определен, подавляет предупреждения Microsoft.
#define _CRT_SECURE_NO_WARNINGS

#include <cstddef>	// Для size_t.

// Строка с базовым управленеим памятью и поддержкой естественных операций.
class String
{
protected:
	char *data;
	size_t length;
	void concatenateWith(const char *rightSide, size_t rightSideLength);
	virtual void zero();
public:
	String() : data(0), length(0) { }
	String(const char *data);
	String(const char *data, const size_t length);
	String(const String& copiedOne);
	String(String&& movedOne);
	virtual ~String();
public:
	char* getData() const { return data; }
	void setData(const char *data);
	const size_t getLength() const { return length; }
	void clear();
public:
	const char operator[](size_t index) const;
	virtual String& operator=(const String& rightSide);
	virtual String& operator=(String&& rightSide);
	bool operator==(const String& other) const;
	const String& operator+=(const String& rightSide);
	const String& operator+=(const char *rightSide);
	operator bool() const;

	// Следующие функции-операторы не являются членами класса.
	friend bool operator==(const String& object, const char *sequence);
	friend bool operator==(const char *sequence, const String& object);
	friend const String operator+(const String& left, const String& right);
private:
	virtual void moveFrom(String&& movedOne);
};


// Строка с возможностью поиска и замены подстрок.
class AdvancedString : public String
{
public:
	AdvancedString() : String() { }
	AdvancedString(const char *data) : String(data) { }
	AdvancedString(const AdvancedString& copiedOne) : String(copiedOne) { }
	AdvancedString(AdvancedString&& movedOne) : String(movedOne) { }
public:
	virtual int indexOf(const String& substring) const;
	virtual void replaceFirst(const String& what, const String& with);
};


// Строка с быстродействующей реализацией поиска подстроки.
class FastSearchString : public AdvancedString
{
private:
	int *failureFunction;
	void clearFailureFunction();
	void rebuildFailureFunction();
	virtual void zero() override;
	void copyFrom(const FastSearchString& source);
	void moveFrom(FastSearchString&& source);
public:
	FastSearchString() : AdvancedString(), failureFunction(0) { }
	FastSearchString(const char *data);
	FastSearchString(const FastSearchString& copiedOne);
	FastSearchString(FastSearchString&& movedOne);
	virtual ~FastSearchString();
public:
	virtual FastSearchString& operator=(const FastSearchString& copiedOne);
	virtual FastSearchString& operator=(FastSearchString&& movedOne);
public:
	virtual int indexOf(const String& substring) const override;
};

#endif