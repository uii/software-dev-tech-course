/* 
   Стандартный заголовочный файл <cassert> включает специальный макрос
   assert(), предназначенный для отладочной(!) проверки условий.
   Если условие, переданное assert() в качестве аргумента, ложно,
   во время выполнения программы выводится сообщение с именем файла,
   номером строки и переданным выражением.
   Если определить макрос NDEBUG перед включением <cassert>,
   макрос assert() ничего не будет делать (так поступают в финальной версии).
*/
#include <cassert>

#include <cstring>	// Для strcmp().
#include <cstdio>

#include "String.h"

// Препроцессор заменит во всем тексте программы имя макроса (FIRST)
// на его подстановку ("first"). Так в С определяли константы.
// Макросы принято записывать большими буквами.
#define FIRST	"first"
#define SECOND	"second"
#define EMPTY	""

// FIRST SECOND раскроется в "first" "second", а идущие подряд литералы
// соединяются в один, и получится "firstsecond", на которую далее
// будут заменяться все вхождения макроса COMBINED.
#define COMBINED	FIRST SECOND

// При помощи typedef объявляется псевдоним TestedString.
// Аналог в Delphi: type TestedString = FastSearchString;
typedef FastSearchString TestedString;

// Эта функция нужна просто для того, чтобы получить rvalue 
// объекта строки и проверить работу конструктора перемещения.
TestedString createString(char *data)
{
	return TestedString(data);
}

int main() 
{ 
	// Составной оператор ограничивает область видимости.
	// В данном случае он удобен тем, что позволяет отследить
	// (в учебных целях) вызовы деструкторов объектов внутри { }.
	{
		TestedString s1(FIRST);
		TestedString s2(SECOND);
		TestedString s3(s2);
		TestedString s4 = createString(s1.getData());

		// Проверка, что конструктор создал именно нужную строку.
		assert(strcmp(s1.getData(), FIRST) == 0);
		assert(s1.getLength() == strlen(FIRST));

		// Проверка правильности работы конструктора перемещения.
		assert(s1 == s4);
		
		// Проверка правильности работы конструктора копирования.
		assert(s2 == s3);
		
		// Проверка правильности работы и коммутативности сравнения. 
		assert(FIRST == s1);
		assert(s1 == FIRST);
		
		// Проверка оператора сцепления строк.
		assert(s1 + s2 == COMBINED);

		// Проверка метода поиска подстроки.
		assert(s1.indexOf(FIRST) == 0);
		assert(s1.indexOf(SECOND) < 0);
		assert(s1.indexOf(s4) == 0);
		assert(s1.indexOf(s2) < 0);

		// Проверка правильности работы оператора сцепления строк.
		assert((s1 += SECOND) == COMBINED);
		assert(s1 == COMBINED);
		assert((s4 += s2) == COMBINED);
		assert(s4 == COMBINED);

		// Проверка правильности работы метода замены подстроки.
		s4.replaceFirst(FIRST, EMPTY);
		assert(s4 == SECOND);
		s2.replaceFirst(FIRST, "");
		assert(s2 == SECOND);

		s1 = createString(s2.getData());
		assert(s1 == s2);
	}

	return 0;
}