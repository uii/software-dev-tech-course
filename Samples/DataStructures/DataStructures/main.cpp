#include <iostream>
#include <cassert>

struct linked_list_node_t
{
	int data;
	linked_list_node_t *next;
};

typedef linked_list_node_t linked_list_t;

void initialize(linked_list_node_t *node, int data = 0)
{
	node->next = 0;
	node->data = data;
}

void destroy(linked_list_t *list)
{
	while (list) 
	{
		linked_list_node_t *node = list;
		list = list->next;
		delete node;
	}
}

// ������� ����� ���������� �������� which ������� ���� node.
// ����:
//	���� which, �� nullptr;
//	���� node.
// �����:
//	����������� ���� (node).
linked_list_node_t *insert_after(linked_list_node_t *which, linked_list_node_t *node)
{
	node->next = which->next;
	which->next = node;
	return node;
}

// ������� ����� ���������� �������� which ������ ���� � ��������� ������� item.
// ����:
//	���� which, �� nullptr;
//	������ item.
// �����:
//	����������� ����.
linked_list_node_t *insert_after(linked_list_node_t *which, int item)
{
	linked_list_node_t *node = new linked_list_node_t();
	initialize(node, item);
	return insert_after(which, node);
}

linked_list_node_t *insert_before(linked_list_t *list, linked_list_node_t *before, linked_list_node_t *node)
{
	node->next = before;
	while (list && list->next != before)
	{
		list = list->next;
	}
	if (list)
	{
		list->next = node;
	}
	return node;
}

linked_list_node_t *insert_before(linked_list_t *list, linked_list_node_t *before, int item)
{
	linked_list_node_t *node = new linked_list_node_t();
	initialize(node, item);
	return insert_before(list, before, node);
}

linked_list_node_t *find(linked_list_t *list, int item)
{
	while (list && list->data != item)
	{
		list = list->next;
	}
	return list;
}

linked_list_node_t *remove(linked_list_t *list, linked_list_node_t *node)
{
	if (!node) 
	{
		return 0;
	}

	while (list && list->next != node)
	{
		list = list->next;
	}
	if (list)
	{
		list->next = node->next;
		return node;
	}
	return 0;
}

linked_list_node_t *remove(linked_list_t *list, int item)
{
	// �������������, ���� ������� ��� ��������� ������:
	linked_list_node_t *node = find(list, item);
	return remove(list, node);
	
	// ����� ����������� ������:
	while (list 
		&& list->next				 // ����������, ����� ������������� nullptr � UB!
		&& list->next->data != item) // � ������� ������ ��� ������: "�������� ���?". 
	{
		list = list->next;
	}
	if (list->next)
	{
		linked_list_node_t *node = list->next;
		list->next = node->next;
		return node;
	}
	return 0;
}

bool erase(linked_list_t *list, linked_list_node_t *node)
{
	linked_list_node_t *victim = remove(list, node);
	if (victim)
	{
		delete victim;
		return true;
	}
	return false;
}

linked_list_node_t *get_list_head(linked_list_t *list)
{
	while (list->next)
	{
		list = list->next;
	}
	return list;
}

linked_list_t *push_to_tail(linked_list_t *list, linked_list_node_t *node)
{
	node->next = list;
	return node;
}

linked_list_t *push_to_tail(linked_list_t *list, int item)
{
	linked_list_node_t *node = new linked_list_node_t();
	initialize(node, item);
	return push_to_tail(list, node);
}

linked_list_node_t *push_to_head(linked_list_t *list, linked_list_node_t *node)
{
	linked_list_node_t *head = get_list_head(list);
	head->next = node;
	node->next = 0;
	return node;
}

linked_list_node_t *push_to_head(linked_list_t *list, int item)
{
	linked_list_node_t *node = new linked_list_node_t();
	initialize(node, item);
	return push_to_head(list, node);
}

linked_list_node_t *pop_from_head(linked_list_t *list)
{
	while (list && list->next && list->next->next)
	{
		list = list->next;
	}
	if (list)
	{
		linked_list_node_t *head = list->next;
		list->next = 0;
		return head;
	}
	return 0;
}

void print_list(linked_list_t *list)
{
	std::cout << "[";
	while (list->next)
	{
		std::cout << list->data << ", ";
		list = list->next;
	}
	std::cout << list->data;
	std::cout << "]" << std::endl;
}

int get_list_length(linked_list_t *list)
{
	int length = 0;
	while (list)
	{
		list = list->next;
		++length;
	}
	return length;
}

int main(int argc, char *argv[])
{
	linked_list_node_t *list = new linked_list_node_t();
	initialize(list);
	assert(get_list_length(list) == 1);

	linked_list_node_t *node = list;
	node = insert_after(node, 1);
	node = insert_after(node, 2);
	node = insert_after(node, 3);
	node = insert_after(node, 5);
	assert(get_list_head(list) == node);
	assert(node->data == 5);
	assert(get_list_length(list) == 5);

	insert_before(list, node, 4);
	assert(get_list_length(list) == 6);
	assert(get_list_head(list)->data == 5);

	print_list(list);

	node = find(list, -1);
	assert(!node);
	node = find(list, 3);
	assert(node);
	assert(node->data == 3);

	assert(erase(list, node));
	assert(!erase(list, 0));
	assert(get_list_length(list) == 5);

	print_list(list);
	
	node = insert_after(find(list, 2), 3);

	print_list(list);
	
	std::cout << get_list_head(list)->data << std::endl;
	std::cin.get();
	
	destroy(list);
}