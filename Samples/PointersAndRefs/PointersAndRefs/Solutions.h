#pragma once

// ��� ������� ��������������� ��������� ��� ������������.
// � ����� ���, ����� ����� ���� ������������ �� �������, ����������� �� *.cpp.
size_t getStringLength(char* string);
bool areStringsEqual(char *first, char *second);
void concatenateStrings(char *first, char *second, char *result);
void copyString(const char* const source, char* const destination);

char* removeRange(char *string, int from, int to, char *result);
char* reverseString(char *string);

// ����� ��������� � ������. "�������" ���������� �� O(N^2).
char* findSubstringNaively(char *which, char *where);