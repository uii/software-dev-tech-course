#include "Solutions.h"

char* findSubstringNaively(char *which, char *where)
{
	// �������� ����� ��������� � ������ ������� � ������.
	for (char *searchSubstring = where; *searchSubstring != '\0'; searchSubstring++)
	{
		// ���������� ������� ������, ������� � ������� �������, � ����������.
		int indexInSubstring = 0;
		while (where[indexInSubstring] != '\0')
		{
			// ���� �����-�� ������ �� ������, ���������� ���������.
			if (which[indexInSubstring] != searchSubstring[indexInSubstring])
			{
				break;
			}
			indexInSubstring++;
		}

		// ���� � �������� ������ ��������� ����� ���������, ��� ������� �������.
		if (which[indexInSubstring] == '\0')
			return searchSubstring;

		// ����� ���������� �����, �������� �� ����������� � ����������.
		// � ����� � �� ���������� - ������ �� ��������.
		searchSubstring += indexInSubstring;
	}
	return 0;
}