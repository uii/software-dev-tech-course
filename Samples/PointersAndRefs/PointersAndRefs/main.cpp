#include <cstdio>
#include <cassert>

#include "Solutions.h"

//	���������� ����� ������.
//	���������:
//		char* string - ������� - ������, ����� ������� ���������� ���������.
//	���������� ����� ������� ������.
size_t getStringLength(char* string)
{
	// ���� ��������� �� ������� ������ �������, ������ ��������� ������ (����� 0).
	if (0 == string)
		return 0;

	size_t length = 0;	// ������� ����� ������.

	// ���� ��������� ������ ������� ������ �� �������, ����������� ������� �����.
	while (string[length] != '\0')
	{
		length++;
	}

	return length;
}


//	�������� ����� �� ���������.
//	���������:
//		char* first  - ������� - ������ ������;
//		char* second - ������� - ������ ������.
//	���������� true, ���� ������ ����������� �����, � false, ���� �� �����.
bool areStringsEqual(char *first, char *second)
{
	// ���� ���� �� ����� - ������� ���������, �� ������ ��������� �������
	// ����� � ������ �����, ����� ��� �������� �������� �����������.
	if (0 == first || 0 == second)
	{
		return first == second;
	}

	size_t index = 0;	// ������� �������� ������� � ���������.

	// ������ �� �������� �����, ���� �� ��������� ����� �����.
	// ������, ����� ����� ����� �� ����� ����������� ������, ��������������
	// �������� �� ��������� �������� ������ �����.
	for (index = 0; first[index] != '\0' && second[index] != '\0'; index++)
	{
		// ���������� ������� ������ � ���� �������.
		if (first[index] != second[index])
		{
			return false; // ���� ������� �� �����, ������ �� �����.
		}
	}

	//	����� ������������� ��������� ��������� ������, ��� ��� ���� ���
	//	����������� � ���� �������:
	//		�) ������������ ��������� ����� ����� �����.
	//		�) ������� ������� �� �����.
	return first[index] == second[index];
}


//	���������� ���� ����� � ����.
//	���������:
//		char* first	 - �������	- ������ ������;
//		char* second - �������	- ������ ������;
//		char* result - �������� - ������-���������.
//	������� ������ ��� ������-���������� ������ ���� ���������� ������,
//	����� �������� ��������� ���������� �����.
void concatenateStrings(char *first, char *second, char *result)
{
	size_t firstIndex;	// ������ ������� � ������ ������.
	size_t secondIndex;	// ������ ������� �� ������ ������.
	
	// ����������� ���������� ������� ������ ������ � ������-���������,
	// ���� ���� ��������� ������ �� �������.
	for (firstIndex = 0; first[firstIndex] != '\0'; firstIndex++)
	{
		result[firstIndex] = first[firstIndex];
	}
	
	// ����������� ���������� ������� ������ ������ � ������-���������,
	// ������� � ���� �����, ��� ����������� ����� ������ ������ � ����������.
	for (secondIndex = 0; second[secondIndex] != '\0'; secondIndex++)
	{
		result[firstIndex + secondIndex] = second[secondIndex];
	}

	// ���������� � ����� ������-���������� ������������ �������� �������.
	result[firstIndex + secondIndex] = '\0';
}


//	����������� ������.
//	���������:
//		char* source	  - �������  - ������-��������, �� ������� ���������� �������.
//		char* destination - �������� - ������-��������, � ������� ���������� �������.
//	������-�������� ������ ��������� �� ������� ������ ���� ����������� �����, 
//	����� �������� ��� ������� ���������.
void copyString(const char* const source, char* const destination) 
{
	size_t index;	// ������� �������� ����������� �������.

	// ������ �� ���� �������� ������-���������, ���� ������� ������ �� �������.
	for (index = 0; source[index] != '\0'; index++)
	{
		destination[index] = source[index];	// ����������� ������� �� ��������� � ��������.
	}

	// ����������� � ����� ��������� �������� �������.
	destination[index] = '\0';
}

int main(int argc, char **argv)
{
	// �������� ������������ ������ getStringLength().
	assert(getStringLength(0) == 0);
	assert(getStringLength("0123456789") == 10);

	// �������� ������������ ������ areStringsEqual().
	assert(areStringsEqual(0,			0)			== true);
	assert(areStringsEqual(0,			"test")		== false);
	assert(areStringsEqual("example",	"example")	== true);
	assert(areStringsEqual("example",	"test")		== false);
	assert(areStringsEqual("test",		"example")	== false);

	char *first = "This is a simple";
	char *second = " test!";
	char *digits = "0123456789";
	char result[256];

	concatenateStrings(first, second, result);
	puts(result);
	assert(getStringLength(first) + getStringLength(second) == getStringLength(result));
	assert(areStringsEqual(result, "This is a simple test!"));

	copyString(first, result);
	assert(areStringsEqual(first, result));

	// ����� �� removeRange():

	// ���������� ���������� ����� ������.
	assert(areStringsEqual(removeRange(digits, 1, 9, result), "09"));
	// �������� ������ �� ��������.
	assert(areStringsEqual(digits, "0123456789"));
	// ��������� �������������� ������ ������ ������.
	assert(getStringLength(removeRange("", 0, 0, result)) == 0);
	// ������������ ������� ��������� ��� ������������ ����������.
	assert(removeRange("0124", 3, 2, result) == 0);
	assert(removeRange("", 0, 2, result)	 == 0);

	copyString(digits, result);
	assert(areStringsEqual(reverseString(result), "9876543210"));
	copyString(digits, result);
	assert(areStringsEqual(reverseString(reverseString(result)), digits));

	assert(findSubstringNaively(digits, "") == 0);
	assert(areStringsEqual(findSubstringNaively("6", digits), "6789"));

	getchar();
	return 0;
}