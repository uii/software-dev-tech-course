#include "Solutions.h"

char *removeRange(char *string, int from, int to, char* result)
{
	// ���������� ������������ ������.
	if (from < 0 || to < 0 || from > to)
		return 0;

	// ������, ����� ������ ������� �� ���������.
	if (from == to)
		return string;
	
	// �������� ���������� from � to � �������� ������.
	int length = getStringLength(string);
	if (from >= length || to >= length)
		return 0;

	for (int i = 0; i < from; i++)
		result[i] = string[i];
	for (int i = to; i <= length; i++)
		result[i + from - to] = string[i];
	return result;
}