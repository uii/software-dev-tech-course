#include "Solutions.h"

char *reverseString(char *string)
{
	size_t length = getStringLength(string);
	for (size_t i = 0; i < length / 2; i++)
	{
		char buffer = string[i];
		string[i] = string[length - i - 1];
		string[length - i - 1] = buffer;
	}
	return string;
}