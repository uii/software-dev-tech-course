#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace std;

void Swap (int *array, size_t first, size_t second)
{
    int dummy = array[first];
    array[first] = array[second];
    array[second] = dummy;
}

void InsertionSort (int *array, size_t len)
{
    for (size_t i = 1; i < len; ++i) {
        size_t j = i;
        while (j > 0 && array[j] < array[j-1]) {
            Swap(array, j, j - 1);
            --j;
        }
    }
}

void SelectionSort (int *array, size_t len)
{
    for (size_t i = 0; i < len - 1; ++i) {
        size_t min = i;
        for (size_t j = i + 1; j < len; ++j) {
            if (array[j] < array[min])
                min = j;
        }
        Swap(array, i, min);
    }
}

void BubbleSort (int *array, size_t len)
{
    for (size_t i = 0; i < len - 1; ++i) {
        bool swapped = false;
        for (size_t j = len - 1; j > i; --j) {
            if (array[j] < array[j-1]) {
                Swap(array, j, j - 1);
                swapped = true;
            }
        }
        if (!swapped) break;
    }
}

void ShellSort (int *array, size_t len)
{
    size_t gap = 1;
    while (gap < len) gap = 3*gap + 1;

    while (gap > 0) {
        gap = gap / 3;

        for (size_t i = gap; i < len; ++i) {
            int dummy = array[i];
            size_t j = i;
            while (j >= gap && array[j - gap] > dummy)	{
                array[j] = array[j - gap];
                j -= gap;
            }
            array[j] = dummy;
        }
    }
}


void MergeSort_recursion
(int *array, size_t len, int * tmp)
{
    if (len < 2) return;

    size_t middle = len / 2;
    MergeSort_recursion(array, middle, tmp);
    MergeSort_recursion(array + middle, len - middle, tmp);
    memcpy(tmp, array, middle * sizeof(array[0]));

    size_t i = 0, j = middle, k = 0;
    while (i < middle && j < len)
        array[k++] = (array[j] < tmp[i])?array[j++]:tmp [i++];
    while (i < middle)
        array[k++] = tmp [i++];
}

void MergeSort(int *array, size_t len)
{
    const size_t half_len = (len + 1) / 2;
    int *tmp = new int[half_len];
    MergeSort_recursion(array, len, tmp);
    delete[] tmp;
}

void HeapSort_sift (int *array, size_t root, size_t len)
{
    while (root * 2 + 1 < len) {
        size_t child_l = 2 * root + 1;
        size_t child_r = 2 * root + 2;
        size_t child   = child_l;
        if (child_r < len && array[child_l] < array[child_r])
            child = child_r;

        if (array[root] < array[child]) {
            Swap(array, child, root);
            root = child;
        }
        else return;
    }
}

void HeapSort(int *array, size_t len)
{
    for (size_t i = len/2 - 1; i != (size_t)-1; --i) {
        HeapSort_sift(array, i, len);
    }

    for (size_t i = len - 1; i > 0; --i) {
        Swap(array, 0, i);
        HeapSort_sift(array, 0, i);
    }
}

size_t RandRange(size_t start, size_t end)
{
    return rand() % (end - start) + start;
}

void QuickSort(int *array, size_t len)
{
    Swap(array, 0, RandRange(1, len));

    size_t pivot = 0;
    for (size_t i = 1; i < len; ++i)
        if (array[i] < array[0]) Swap(array, ++pivot, i);
    Swap(array, 0, pivot);

    if (pivot > 1)
        QuickSort(array, pivot);
    if ((pivot + 2) < len)
        QuickSort(array + pivot + 1, len - pivot - 1);
}


clock_t timer;

void InitArray(int *array, int *init_array, size_t len)
{
    for (size_t i = 0; i < len; ++i)
        array[i] = init_array[i];

    timer = clock();
}

void CheckArray (int *array, size_t len, const char *sort_name)
{
    float sort_time = (float)(clock() - timer)/CLOCKS_PER_SEC;
    bool is_ok = true;

    for (size_t i = 1; i < len; ++i) {
        if (array[i] < array[i-1]) {
            is_ok = false;
        }
    }

    cout << sort_name << " (" << sort_time << " seconds) is ";
    if (is_ok)
        cout << "OK" << endl;
    else
        cout << "ERROR" << endl;
}

int main()
{
    const size_t len = 20000;
    int *init_array = new int[len];
    int *array      = new int[len];

    for (size_t i = 0; i < len; ++i)
        init_array[i] = (int)rand();

    InitArray(array, init_array, len);
    InsertionSort(array, len);
    CheckArray(array, len, "Insertion sort");

    InitArray(array, init_array, len);
    SelectionSort(array, len);
    CheckArray(array, len, "Selection sort");

    InitArray(array, init_array, len);
    BubbleSort(array, len);
    CheckArray(array, len, "Bubble sort");

    InitArray(array, init_array, len);
    ShellSort(array, len);
    CheckArray(array, len, "Shell sort");

    InitArray(array, init_array, len);
    MergeSort(array, len);
    CheckArray(array, len, "Merge sort");

    InitArray(array, init_array, len);
    HeapSort(array, len);
    CheckArray(array, len, "Heap sort");

    InitArray(array, init_array, len);
    QuickSort(array, len);
    CheckArray(array, len, "Quick sort");

    delete[] array;
    delete[] init_array;

    return 0;
}

